<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

		<!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<![endif]--><?php $setting=$db->get_row('setting',array('id'=>1));?>
        <?php $get_banner=$db->get_row('banner',array('id'=>1));?>
		<title><?php echo $setting['title'];?></title>
        
		<meta name="description" content="<?php echo $setting['description'];?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="<?php echo $setting['keywords'];?>"/>

		<meta property="og:title" content="Restaurante" />
		<meta property="og:description" content="Restaurante Landing Page." />		

			<link rel="stylesheet" href="<?php echo SITE_URL.'/assets/website/css/bootstrap.min.css';?>">
		<link rel="stylesheet" href="<?php echo SITE_URL.'/assets/website/css/flexslider.css';?>">
		<link rel="stylesheet" href="<?php echo SITE_URL.'/assets/website/css/jquery.jscrollpane.custom.css';?>">
		<link rel="stylesheet" href="<?php echo SITE_URL.'/assets/website/css/jquery.bxslider.css';?>">
		<link rel="stylesheet" href="<?php echo SITE_URL.'/assets/website/css/lightbox.css';?>">
		<link rel="stylesheet" href="<?php echo SITE_URL.'/assets/website/css/font-awesome.min.css';?>">
		<link rel="stylesheet" href="<?php echo SITE_URL.'/assets/website/css/buttons.css';?>">
		<link rel="stylesheet" href="<?php echo SITE_URL.'/assets/website/css/styles.css';?>">

	
    </head>
    <body data-spy="scroll" data-offset="200" id="sTop">


			<div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
			  <div class="container">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#b-menu-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<div class="collapse navbar-collapse" id="b-menu-1">
				  <ul class="nav navbar-nav navbar-right">
					<li><a href="#sTop">HOME</a></li>
					<li><a href="#s0">EVENT</a></li>
					<li><a href="#s1">MENU</a></li>
					
					<li><a href="#s2">TESTIMONIAL</a></li>
					<li><a href="#s3">ABOUT</a></li>
					<li><a href="#s4">GALLERY</a></li>
					
					<li><a href="#s6">CONTACT</a></li>
				  </ul>
				</div> <!-- /.nav-collapse -->
			  </div> <!-- /.container -->
			</div> <!-- /.navbar -->	

		<header>
			<div class="container topbar">
				<div class="logo">
				<img src="<?php echo SITE_URL.'/uploads/logo/'.$setting['logo'];?>"  /></div>
				
				<div class="contact"><span class="ebold"><?php echo $setting['name'];?>,</span> <?php echo $setting['address'];?> |  <?php echo $setting['phone_no'];?> | <a href="mailto:address@example.com"><?php echo $setting['email'];?></a></div>
			</div>
		</header>
