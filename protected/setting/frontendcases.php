<?php
if (! isset($query1ans) || $query1 == '' || $query1ans == '') {
    $query1 = frontend;
    $query1ans = 'home';
}
$fcontroller = SERVER_ROOT . '/protected/controller/frontend/' . $query1ans . '_controller.php';
$fview = SERVER_ROOT . '/protected/views/frontend/' . $query1ans . ".php";
if ($query1ans == "login" ||  $query1ans == ''  ) {
    if (file_exists($fview)) {
        if (file_exists($fcontroller))
            require $fcontroller;
        require $fview;
    }
} elseif ($query1ans == "logout") {
    setcookie('remember_me', "", time() - 3600);
    $session->destroy('login', fview);
} else {
    if (file_exists(SERVER_ROOT . '/protected/setting/frontend/common_data.php')) {
        
        require SERVER_ROOT . '/protected/setting/frontend/common_data.php';
    }
    if (file_exists(SERVER_ROOT . '/protected/setting/frontend/header.php')) {
        if($query1ans!='pdfgenerate')
        require SERVER_ROOT . '/protected/setting/frontend/header.php';
    }
    if (file_exists(SERVER_ROOT . '/protected/setting/frontend/sidebar.php')) {
        require SERVER_ROOT . '/protected/setting/frontend/sidebar.php';
    }
    if (file_exists($fview)) {
        if (file_exists($fcontroller))
            require $fcontroller;
        require $fview;
    }
    
    if (file_exists(SERVER_ROOT . '/protected/setting/frontend/footer.php')) {
        require SERVER_ROOT . '/protected/setting/frontend/footer.php';
    }
}
?>