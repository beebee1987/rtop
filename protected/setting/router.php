<?php
/* directory that contain classes */
$classesDir = array(
    SERVER_ROOT . '/protected/library/'
);
/* loading all library components in everywhere */
spl_autoload_register(function ($class)
{
    global $classesDir;
    foreach ($classesDir as $directory) {
        if (file_exists($directory . $class . '_class.php')) {
            require ($directory . $class . '_class.php');
            
            return;
        }
    }
});
/* loading all library end */
/* Connect to an ODBC database using driver invocation */
// $db = new db("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
$db = new search("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);

$fv = new form_validations();
$feature = new feature();
$password = new password();
$link = new links();
$session = new session();
$mail = new mandrill_mail();

/**
 * This controller routes all incoming requests to the appropriate controller and page
 */

$request = explode('?', $_SERVER['REQUEST_URI']);
$parsed = explode('=', $request['1']);
$query3ans = $parsed['3'];

$query1 = $parsed['0'];
$getParsed = explode('&', $parsed['1']);

$query1ans = $getParsed['0'];
$query2 = $getParsed['1'];
$query2ans = $parsed['2'];
$query2ans_extended = explode('&', $query2ans);
$query2ans = $query2ans_extended['0'];
$query3 = $query2ans_extended['1'];

if ($query1 == frontend) {
    require SERVER_ROOT . '/protected/setting/frontendcases.php';
} else {
    require SERVER_ROOT . '/protected/setting/websitecases.php';
}
?>