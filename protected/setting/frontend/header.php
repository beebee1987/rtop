<?php 
if(!$session->Check())
{
$session->destroy('login',frontend);
}
elseif($_SESSION['role']!='Administrator')
{
    $session->destroy('login',frontend);
}
?>

<!DOCTYPE html>
<html>
<head>
<title> <?php  $title=$db->get_row('setting',array('id'=>1));
      echo $title['name']; ?>
      </title>
	<link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" /> 
    <link href="<?php echo SITE_URL.'/assets/frontend/css/bootstrap.min.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/font-awesome.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="<?php echo SITE_URL.'/assets/frontend/css/se7en-font.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/isotope.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/jquery.fancybox.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/fullcalendar.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/wizard.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/select2.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/morris.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/datatables.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/datepicker.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/timepicker.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/colorpicker.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/bootstrap-switch.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/daterange-picker.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/typeahead.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/summernote.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/pygments.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/style.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/color/green.css';?>" media="all" rel="alternate stylesheet" title="green-theme" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/color/orange.css';?>" media="all" rel="alternate stylesheet" title="orange-theme" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/color/magenta.css';?>" media="all" rel="alternate stylesheet" title="magenta-theme" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/color/gray.css';?>" media="all" rel="alternate stylesheet" title="gray-theme" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/jquery.fileupload-ui.css';?>" media="all" rel="alternate stylesheet" title="gray-theme" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/ladda-themeless.min.css';?>" media="all" rel="stylesheet" type="text/css" />
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
  </head>
<div class="navbar navbar-fixed-top scroll-hide">
        <div class="container-fluid top-bar">
        <button class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
         <b class="logo">
         <?php  $title=$db->get_row('setting',array('id'=>1));
      echo $title['name'];?></b>
          <div class="pull-right">
            <ul class="nav navbar-nav pull-right">
              <li class="dropdown user hidden-xs"><a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <b>Hi,</b>&nbsp;&nbsp; <?php echo $_SESSION['name']; ?> 
              <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?php echo $link->link('logout',frontend);?>" ><i class="icon-signout"></i>Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          
        </div>
        <div class="container-fluid main-nav clearfix">
          <div class="nav-collapse">
           <ul class="nav">
            
            <li>
                <a  <?php if($query1ans=="home"){?> class="current" <?php }?> href="<?php echo $link->link('home',frontend);?>"><span class="icon-home" aria-hidden="true"></span>Home</a>
               
              </li>
              <li>
                <a <?php if($query1ans=="editor"){?> class="current" <?php }?> href="<?php echo $link->link('editor',frontend);?>"><span class="icon-heart" aria-hidden="true"></span>About Us</a>
               
              </li>
             <li>
                <a  <?php if($query1ans=="foodlist"){?> class="current" <?php }?> href="<?php echo $link->link('foodlist',frontend);?>"><span class="icon-food" aria-hidden="true"></span>Food List</a>
               
              </li>
             <li class="dropdown">
                <a  <?php if($query1ans=="table" || $query1ans=="bookings" || $query1ans=="completed_bookings"){?> class="current" <?php }?>  data-toggle="dropdown" href="#">
                <span class="icon-table" aria-hidden="true"></span>
                Bookings/Tables <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?php echo $link->link('table',frontend);?>">Manage Tables</a>
                  </li>
                  <li>
                    <a href="<?php echo $link->link('bookings',frontend);?>">Pending Bookings</a>
                  </li>
                  <li>
                    <a href="<?php echo $link->link('completed_bookings',frontend);?>">Completed Bookings</a>
                  </li>
                 
              
            </ul>  
              
              
               
              </li>
              
                <li class="dropdown" >
               <a  <?php if($query1ans=="order_step1" || $query1ans=="invoices" ){?> class="current" <?php }?> data-toggle="dropdown" href="#">
                <span aria-hidden="true" class="icon-dollar" ></span>POS/Invoices<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?php echo $link->link('order_step1',frontend);?>">Add Invoices</a>
                  </li>
                  <li>
                    <a href="<?php echo $link->link('invoices',frontend);?>">View Invoices</a>
                  </li>
                 
              
            </ul> </li>
             
             <li>
                <a  <?php if($query1ans=="event"){?> class="current" <?php }?> href="<?php echo $link->link('event',frontend);?>"><span class="icon-calendar" aria-hidden="true"></span>Events</a>
               
              </li>
              <li>
                <a <?php if($query1ans=="testimonial"){?> class="current" <?php }?>   href="<?php echo $link->link('testimonial',frontend);?>"><span class="icon-male" aria-hidden="true"></span>Testimonials</a>
               
              </li>
               <li>
                <a <?php if($query1ans=="user"){?> class="current" <?php }?> href="<?php echo $link->link('user',frontend);?>"><span class="icon-group" aria-hidden="true"></span>Users</a>
               
              </li>

                      </li>
               
                <li>
                <a <?php if($query1ans=="gallery"){?> class="current" <?php }?> href="<?php echo $link->link('gallery',frontend);?>"><span class="icon-picture" aria-hidden="true"></span>Gallery</a>
               
              </li>
               <li class="dropdown" >
               <a  <?php if($query1ans=="website_setting" || $query1ans=="social" ||$query1ans=="banner_setting"){?> class="current" <?php }?> data-toggle="dropdown" href="#">
                <span aria-hidden="true" class="icon-cogs" aria-hidden="true"></span>Setting<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?php echo $link->link('website_setting',frontend);?>">Website Setting</a>
                  </li>
                  <li>
                    <a href="<?php echo $link->link('banner_setting',frontend);?>">Banner Setting</a>
                  </li>
                  <li>
                    <a href="<?php echo $link->link('social',frontend);?>">Social Media</a>
                  </li>
              
            </ul> </li>
            <li>
                <a  <?php if($query1ans=="queries"){?> class="current" <?php }?> href="<?php echo $link->link('queries',frontend);?>"><span class="icon-question" aria-hidden="true"></span>Queries</a>
               
              </li>
            </ul>
          </div>
        </div>
      </div>
      