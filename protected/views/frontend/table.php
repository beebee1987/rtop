<div class="container-fluid main-content">
<div class="page-title">
<h2>Manage Tables</h2></div>
 
<div class="row">
 <div class="col-md-12">
<?php echo $display_msg; ?>

<div class="widget-container fluid-height clearfix">
 <div class="widget-content padded">
 
 <div class="row">
 <div class="col-md-12">
 
  <div class="col-md-3"> 
  
  <form method="post" class="form-horizontal" action=""  >
<div class="form-group">
            <label class="control-label col-md-4">RoomNumber</label>
            <div class="col-md-8">
              <select class="form-control" name="room"  >
               <option value="">-----Select Room-----------</option>
                <?php 
                if (is_array($get_room))
                    foreach ($get_room as $room){?>
                    <option value="<?php echo $room['room'];?>" <?php if($_POST['room']==$room['room']){echo "selected";}?> ><?php echo $room['room'];?></option>
			<?php } ?>
              
            </select><br></div>
            <div class="col-md-4"></div><div class="col-md-8">
            <button class="btn   btn-warning" type="submit" name="view"><i class="icon-eye-open"></i>View Rooms</button>
			 </div> <div class="col-md-4"></div><div class="col-md-8">
			 <a class="btn btn-primary btn fancybox" href="#fancybox-example"><i class="icon-plus"></i>Add a New Table</a>
			</div> </div></form></div>
			
		
		
		
       
  <div class="col-md-7">
            <table class="table table-bordered table-striped dataTable" id="dataTable1" >
                  <thead>
                    <tr>
                   
                    <th class="sorting" width="15%" > 
                     Table No.</th>
                    <th class="sorting" width="20%" >
                    Room No.
                    </th><th class="hidden-xs sorting" width="35%" >
                    No. of Places available for Table
                    </th><th class="hidden-xs sorting" width="20%" >
                    Table Position
                    </th>
                    
                    <th class="sorting" width="10%">Action
                    </th>
                    </tr></thead>
                  
                <tbody>
          
<?php
if(is_array($select))
	foreach ($select as $value)
						{?>
                      <tr>                     
                    
                      <td >
                        <?php echo $value['table_no'];?> 
                      </td>
                      <td >
                        <?php echo $value['room'];?> 
                      </td>
                      <td class="hidden-xs">
                        <?php echo $value['available_place'];?> 
                      </td>
                      <td class="hidden-xs" >
                        <?php echo $value['table_pos'];?> 
                      </td>
                     
                      <td class="actions">
                        <div class="action-buttons">                                  
                          <a class="table-actions" href="<?php echo $link->link("edit_table",'frontend','&edit='.$value['id']);?>"><i class="icon-pencil"></i></a>
                          <a class="table-actions" href="<?php echo $link->link("table",'frontend','&del='.$value['id']);?>"><i class="icon-trash"></i></a>
                        </div>
                      </td>                    
                    </tr>
                    <?php }?>
                    </tbody>
                  </table></div>
                  
                  
                    <div class="col-md-2">       
               <h3>Room List</h3>   
                  
                 <table class="table table-bordered">
                  <thead>
                    <tr>
                    
                    <th class="sorting">
                    <b>Room No</b>
                    </th>
                    <th class="sorting"><b>Action</b>
                    </th>
                    </tr></thead>
                  
                <tbody>
               
<?php
if(is_array($select1))
	foreach ($select1 as $value)
						{?>
                      <tr>                     
                      
                   
                      <td class="sorting">
                        <?php echo $value['room'];?> 
                      </td>
                     
                      <td class="actions sorting">
                        <div class="action-buttons">                                  
                   <a class="table-actions" href="<?php echo $link->link("table",'frontend','&del_id='.$value['id']);?>"><i class="icon-trash"></i></a>
                        </div>
                      </td>                    
                    </tr>
                    <?php }?>
                    </tbody>
                  </table></div>
         
                  
                   
                  </div></div></div></div>
           </div>
           
  
     </div></div> 
            
 
 
 
 
 
 
 <!--Start  Add Table -->
               
 <div id="fancybox-example" style="display: none">
 <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
 <div class="row">
  <div class="col-md-12">

 <div class="heading"><h4><b>Table Specification</b></h4></div>



   
    <div class="form-group">
            <label class="control-label col-md-4">Table Number</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="table_no" value="<?php echo $table_no;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     
     
     <div class="form-group">
            <label class="control-label col-md-4">No. of places available for this Table</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="available_place" value="<?php echo $available_place;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     
     <div class="form-group">
            <label class="control-label col-md-4">Table Position</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="table_pos" value="<?php echo $table_pos;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     
    <div class="form-group">
            <label class="control-label col-md-4" >Select ROOM number where the table will be added:</label>
            <div class="col-md-7">
              <div class="input-group">
              <select class="form-control" name="room"  >
               <option value="">------------Select Room--------------------</option>
                <?php $get_room=$db->get_all('room');
                if (is_array($get_room))
                    foreach ($get_room as $room){?>
                    <option value="<?php echo $room['room'];?>" <?php if($_POST['room']==$room['room']){echo "selected";}?> ><?php echo $room['room'];?></option>
			<?php } ?>
              
            </select>
               <span class="input-group-addon"  style="color:red">*Required</span></div>  </div>
           
    </div>
    
    
     <div class="form-group">
            <label class="control-label col-md-4">Or Add a New Room</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="new_room" value="<?php echo $new_room;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     
     <div class="form-group">
          
            <label class="control-label col-md-4"> Select Image</label>
            <div class="col-md-4">
              <div class="fileupload fileupload-new" data-provides="fileupload">
              <input type="hidden" value="" name="photo">
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                  <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px;"></div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Select photo</span>
                  <span class="fileupload-exists">Change</span>
                  <input type="file" name="image"></span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
              </div>
            </div>
          </div> </div>
     
        </div>
         <div class="row">  
            <div class="col-md-12">  
         <div class="form-group">
    <div class="col-md-6">
    <button class="btn btn-lg btn-block btn-success" type="submit" name="submit"><i class="icon-check"></i>Add Table</button>
     </div>
     <div class="col-md-6">
    <button class="btn btn-lg btn-block btn-danger" name="reset" type="reset"><i class="icon-refresh"></i>Reset</button>
    </div> </div></div> </div>
  </form>
  
   
</div>
