<div class="container-fluid main-content">
	<div class="page-title">
		<h2>
			Gallery<a class="btn btn-primary pull-right btn fancybox"
				href="#fancybox-example"><i class="icon-plus"></i>Add Image</a>
		</h2>
	</div>

	<div class="row">
		<div class="col-md-12">
		<?php echo $display_msg;?>
			<div class="widget-container fluid-height">
				<div class="heading">
					
					<div class="gallery-filters list-inline btn-group pull-right">
					  <a class="btn btn-sm btn-primary-outline selected" data-filter="*" href="#">All</a>
					 <?php 
                if (is_array($get_image_category))
                    foreach ($get_image_category as $image_category){?>
						<a class="btn btn-sm btn-primary-outline" data-filter=".<?php echo $image_category['image_category'];?>"
							href=""><?php echo $image_category['image_category']; }?></a>
					</div>
				</div>
<div class="widget-content padded">
<div class="gallery-container isotope" style="position: relative; overflow: hidden; height: 1000px; width: 440px;">
<?php 
 if (is_array($get_image_category))
 foreach ($get_image_category as $image_category){
 $get_image=$db->get_all('image_name',array('image_category'=>$image_category['image_category']));
if (is_array($get_image))
foreach ($get_image as $image){?>

<a  href="<?php echo $link->link("gallery",'frontend','&del='.$image['id']);?>" class="gallery-item <?php echo $image_category['image_category'];?> fancybox isotope-item"  style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1); opacity: 1;">
 <img src="<?php echo SITE_URL.'/uploads/gallery/'.$image_category['image_category'].'/'.$image['image'];?>"> 
   <div class="actions"><i class="fa fa-trash-o"></i></div></a>

			 
	 <?php }}?>
              
        </div>		
	</div>
				</div>
			</div>
		</div>
	</div>






<!-- Add Image -->


<div id="fancybox-example" style="display: none">
			 
    <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
  
 <div class="row">
  <div class="col-md-12">


 <div class="heading"><h4><b>Add Image</b></h4></div>
 
    <div class="form-group">
            <label class="control-label col-md-4">Type a Image Category</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="new_category" value="<?php echo $new_category;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     
       <div class="form-group">
            <label class="control-label col-md-4" >Or Choose Category From Existing Ones:</label>
            <div class="col-md-6">
              <div class="input-group">
              <select class="form-control" name="image_category"  >
               <option value="">--------Select Category--------------------</option>
                <?php $get_image_category=$db->get_all('image_category');
                if (is_array($get_image_category))
                    foreach ($get_image_category as $image_category){?>
                    <option value="<?php echo $image_category['image_category'];?>" <?php if($_POST['image_category']==$image_category['image_category']){echo "selected";}?> ><?php echo $image_category['image_category'];?></option>
			<?php } ?>
              
            </select>
               <span class="input-group-addon"  style="color:red">*Required</span></div>  </div>
           
    </div>
     
      <div class="form-group">
            <label class="control-label col-md-4">Image Name</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="image_name" value="<?php echo $image_name;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
     
  
    <div class="form-group">
          
            <label class="control-label col-md-4">Select Image</label>
            <div class="col-md-4">
              <div class="fileupload fileupload-new" data-provides="fileupload">
             
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                  <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px;"></div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Select Image</span>
                  <span class="fileupload-exists">Change</span>
                  <input type="file" name="image"></span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
              </div>
            </div>
          </div>  
              
            </div>
 </div>
         

 <div class="row">
 <div class="col-md-12">
   <div class="form-group">

<div class="col-md-6">
<button class="btn btn-lg btn-block btn-success" type="submit" name="submit"><i class="icon-check"></i>Add Image</button>
     </div>
     <div class="col-md-6">
<button class="btn btn-lg btn-block btn-danger" name="reset" type="reset"><i class="icon-refresh"></i>Reset</button>
 </div>  </div>  </div> 
</div></form></div>