 <div class="container-fluid main-content">
<div class="page-title">
<h2>Change Contact Information</h2></div>
 
 <div class="row">
  <div class="col-md-12">
   <?php echo $display_msg; ?>
<div class="widget-container fluid-height clearfix">
<div class="widget-content padded">

   <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
  <div class="row">
<div class="col-md-12">
<div class="heading">
<h3>General Information</h3></div>
<div class="col-md-6">

<div class="form-group">
            <label class="control-label col-md-4">Restaurant Name</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="name" value="<?php echo $getdata['name'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
   <div class="form-group">
            <label class="control-label col-md-4">Restaurant Phone Number</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="phone_no" value="<?php echo $getdata['phone_no'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
      <div class="form-group">
            <label class="control-label col-md-4">Currency</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="currency" value="<?php echo $getdata['currency'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
     <div class="form-group">
            <label class="control-label col-md-4">Paypal E-Mail</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="paypal_email" value="<?php echo $getdata['paypal_email'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     <div class="form-group">
           <label class="control-label col-md-4">Enable Online Cart</label>&nbsp;&nbsp;&nbsp;
           <label class="radio-inline">
              <input name="enable" type="radio" value="yes" <?php if($getdata['enable']=="yes"){echo "checked";}?>><span>Yes</span></label>
              <label class="radio-inline">
              <input  name="enable" type="radio" value="no" <?php if($getdata['enable']=="no"){echo "checked";}?>>
              <span>No</span></label>
            </div>
             <div class="form-group">
           <label class="control-label col-md-4">Paypal Live</label>&nbsp;&nbsp;&nbsp;
           <label class="radio-inline">
              <input name="paypal_live" type="radio" value="true" <?php if($getdata['paypal_live']=="true"){echo "checked";}?>><span>Sandbox</span></label>
              <label class="radio-inline">
              <input  name="paypal_live" type="radio" value="false" <?php if($getdata['paypal_live']=="false"){echo "checked";}?>>
              <span>Live</span></label>
            </div>
    
    
     <div class="form-group">
            <label class="control-label col-md-4">Restaurant E-Mail</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="email" value="<?php echo $getdata['email'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
    
     
     
     
          
            <div class="form-group">
            <label class="control-label col-md-4">Address of the Restaurant</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="address" value="<?php echo $getdata['address'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
      
      <div class="form-group">
          
            <label class="control-label col-md-4">Upload Logo</label>
            <div class="col-md-5">
              <div class="fileupload fileupload-new" data-provides="fileupload">
              
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                  <?php if(file_exists(SERVER_ROOT.'/uploads/logo/'.$load.'/'.$getdata['logo']) && (($getdata['logo'])!=''))
              { ?>
    <img src="<?php echo SITE_URL.'/uploads/logo/'.$load.'/'.$getdata['logo'];?>" width="100%">
   <?php } else{?>
              	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image"  width="100%">
             <?php } ?>
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px;"></div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Select photo</span>
                  <span class="fileupload-exists">Change</span>
                  <input type="file" name="logo" ></span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
              </div>
            </div>
          </div>
             
     
    
  </div>
       
     <div class="col-md-6">
    
     
     <div class="form-group">
            <label class="control-label col-md-3">Site Title</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="title" value="<?php echo $getdata['title'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
     <div class="form-group">
          <label class="control-label col-md-3">Site Description</label>
          <div class="col-md-6">
          <div class="input-group">
           <textarea class="form-control" name="description" row="3"><?php echo $getdata['description']; ?></textarea>
                  <span class="input-group-addon"  style="color:red">*Required</span></div>
            </div> </div>
     
     
       <div class="form-group">
            <label class="control-label col-md-3">Site Keywords</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="keywords" value="<?php echo $getdata['keywords'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
        
  <div class="heading"><h3>Open Between</h3></div>
    


   <div class="form-group">
            <label class="control-label col-md-3">Monday</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="mon" value="<?php echo $getdata['mon'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
      <div class="form-group">
            <label class="control-label col-md-3">Tuesday</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="tues" value="<?php echo $getdata['tues'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
      <div class="form-group">
            <label class="control-label col-md-3">Wednesday</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="wed" value="<?php echo $getdata['wed'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
      <div class="form-group">
            <label class="control-label col-md-3">Thursday</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="thurs" value="<?php echo $getdata['thurs'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
      <div class="form-group">
            <label class="control-label col-md-3">Friday</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="fri" value="<?php echo $getdata['fri'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
      <div class="form-group">
            <label class="control-label col-md-3">Saturday</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="sat" value="<?php echo $getdata['sat'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
      <div class="form-group">
            <label class="control-label col-md-3">Sunday</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="sun" value="<?php echo $getdata['sun'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
     


 <!-- 
  <div class="heading"><h3>Location on the Map</h3></div> 
   <div class="form-group">
            <label class="control-label col-md-4">Latitude</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="latitude" value="<?php echo $getdata['latitude'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
    
    
     <div class="form-group">
            <label class="control-label col-md-4">Longitude</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="longitude" value="<?php echo $getdata['longitude'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>  -->
       
             </div>
     
         
          </div> </div>      
          <div class="row">
          <div class="col-md-12">
             <div class="form-group">
             <div class="col-md-12">
           <button class="btn btn-lg btn-block btn-success" type="submit" name="update"><i class="icon-check"></i>Update</button>
                  </div></div></div></div>
                 
           
 </form></div>
 </div></div></div></div>
 



