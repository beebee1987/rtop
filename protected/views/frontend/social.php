 <div class="container-fluid main-content">
<div class="page-title">
<h2>Social Media Links</h2></div>
 
 <div class="row">
  <div class="col-md-12">
   <?php echo $display_msg; ?>
<div class="widget-container fluid-height clearfix">
<div class="widget-content padded">

   <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
  <div class="row">
<div class="col-md-12">
<div class="col-md-3"></div>
<div class="col-md-6">

   <div class="form-group">
            <label class="control-label col-md-4">Facebook URL</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="facebook" value="<?php echo $getdata['facebook'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
    
    
     <div class="form-group">
            <label class="control-label col-md-4">Twitter URL</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="twitter" value="<?php echo $getdata['twitter'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
     
 


   <div class="form-group">
            <label class="control-label col-md-4">Google+ URL</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="google" value="<?php echo $getdata['google'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
    
    
     
           
     <div class="form-group">
            <label class="control-label col-md-4">LinkedIn URL</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="linkedin" value="<?php echo $getdata['linkedin'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
     <div class="form-group">
            <label class="control-label col-md-4">Pinterest URL</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="pinterest" value="<?php echo $getdata['pinterest'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
            
           
            </div>
      </div>    </div>
             
          <div class="row">
          <div class="col-md-12">
<div class="col-md-5"></div>
<div class="col-md-6">
             <div class="form-group">
                <div class="col-md-6">
           <button class="btn btn-lg btn-block btn-success" type="submit" name="update"><i class="icon-check"></i>Update</button>
                  </div></div></div></div></div>
                 
           
 </form></div>
 </div></div></div></div>
 



