 <div class="container-fluid main-content">
<div class="row">
<div class="col-md-12">
<h2>Update Testimonial <a class="table-actions btn btn-primary pull-right" href="<?php echo $link->link('testimonial',frontend);?>">
<i class="icon-backward"></i>Go Back
 </a> </h2></div></div>
 
 <div class="row">
  <div class="col-md-12">
   <?php echo $display_msg; ?>
<div class="widget-container fluid-height clearfix">
<div class="widget-content padded">
   <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
  <div class="row">
<div class="col-md-12">
<div class="col-md-6">

   <div class="form-group">
            <label class="control-label col-md-4">Client Name</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="name" value="<?php echo $getdata['name'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
    
    
     <div class="form-group">
            <label class="control-label col-md-4">Client Job</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="job" value="<?php echo $getdata['job'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
     
      <div class="form-group">
            <label class="control-label col-md-4">Client Work At</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="work_at" value="<?php echo $getdata['work_at'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
     
  
       
    <div class="form-group">
          <label class="control-label col-md-4">Content</label>
          <div class="col-md-7">
          <div class="input-group">
           <textarea class="form-control" name="content" ><?php echo $getdata['content']; ?></textarea>
                  <span class="input-group-addon"  style="color:red">*Required</span></div>
            </div> </div> </div>
     <div class="col-md-6">
       <div class="form-group">
          
            <label class="control-label col-md-3">Select Image</label>
            <div class="col-md-5">
              <div class="fileupload fileupload-new" data-provides="fileupload">
              
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                  <?php if(file_exists(SERVER_ROOT.'/uploads/testimonial/'.$load.'/'.$getdata['image']) && (($getdata['image'])!=''))
              { ?>
    <img src="<?php echo SITE_URL.'/uploads/testimonial/'.$load.'/'.$getdata['image'];?>" width="100%">
   <?php } else{?>
              	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image"  width="100%">
             <?php } ?>
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px;"></div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Select photo</span>
                  <span class="fileupload-exists">Change</span>
                  <input type="file" name="image" ></span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
              </div>
            </div>
          </div>
                     
                  
          </div> </div>    </div>   
            <div class="row">
               <div class="col-md-12">
             <div class="form-group">
             <div class="col-md-12">
            <button class="btn btn-lg btn-block btn-success" type="submit" name="update"><i class="icon-check"></i>Update</button>
                  </div></div></div></div>
                 
           
 </form></div>
 </div></div></div></div>
 



