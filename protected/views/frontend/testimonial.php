<div class="container-fluid main-content">
<div class="row">
 <div class="col-md-12">
<h2>Testimonials List
<a class="btn btn-primary pull-right btn fancybox" href="#fancybox-example"><i class="icon-plus"></i>Add Testimonials</a></h2>
  <?php echo $display_msg; ?></div></div>   
  
      <div class="row">
      <div class="col-md-12">
       <div class="widget-container scrollable list rating-widget">
              <div class="widget-content">
            
                <ul>
    
                <?php
if(is_array($select))
	foreach ($select as $value)
						{?>
				   <li>
				     <div class="row">
                   <div class="col-md-12">
                   <div class="col-md-2">
            <div class="fileupload fileupload-new" data-provides="fileupload">
           <div class="fileupload-new img-thumbnail" style="width: 200px; height: 120px;">
                  <?php if(file_exists(SERVER_ROOT.'/uploads/testimonial/'.$value['id'].'/'.$value['image']) && (($value['image'])!=''))
              { ?>
    <img src="<?php echo SITE_URL.'/uploads/testimonial/'.$value['id'].'/'.$value['image'];?>" width="">
   <?php } else{?>
              	<img  src="http://www.placehold.it/50x50/EFEFEF/AAAAAA&text=no+image"  width="">
             <?php } ?>
                  
                </div> 
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px;"></div>
               </div></div><div class="col-md-10">
               <b><?php echo $value['name'];?></b>
             - <?php echo $value['job']?><b>&nbsp;at</b>
              <?php echo $value['work_at']?><br>
               <?php echo $value['content'];?>
               <div class="action-buttons">                                  
              <a class="btn btn-xs btn-primary" href="<?php echo $link->link("edit_testimonial",'','&edit='.$value['id']);?>"><i class="icon-pencil"></i>Edit</a>
              <a class="btn btn-xs btn-danger" href="<?php echo $link->link("testimonial",'user','&del='.$value['id']);?>"><i class="icon-trash"></i>Delete Testimonial</a>
                       </div> </div>
                      
                   
                 
                    <?php }?>
                  </div> </div>
          </ul> </div>
               
              </div></div></div></div>
            
       
        
        
      
      <!-- Add Client -->
      
      
      <div id="fancybox-example" style="display: none">

     <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
  <div class="row">
  <div class="col-md-12">

 <div class="heading"><h4><b>Add Testimonial</b></h4></div>



   
    <div class="form-group">
            <label class="control-label col-md-4">Client Name</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="name" value="<?php echo $name;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     
     
     <div class="form-group">
            <label class="control-label col-md-4">Client Job</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="job" value="<?php echo $job;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     
     <div class="form-group">
            <label class="control-label col-md-4">Client works At</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="work_at" value="<?php echo $work_at;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     
     
    
     
 
    
    <div class="form-group">
          <label class="control-label col-md-4">Content</label>
          <div class="col-md-7">
          <div class="input-group">
           <textarea class="form-control" name="content" ><?php echo $content; ?></textarea>
                  <span class="input-group-addon"  style="color:red">*Required</span></div>
            </div> </div>
     
     
     <div class="form-group">
          
            <label class="control-label col-md-4"> Select Image</label>
            <div class="col-md-4">
              <div class="fileupload fileupload-new" data-provides="fileupload">
            
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                  <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px;"></div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Select photo</span>
                  <span class="fileupload-exists">Change</span>
                  <input type="file" name="image"></span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
              </div>
            </div>
          </div> 
     
        </div></div>
        <div class="row">
            <div class="col-md-12">  
         <div class="form-group">
    <div class="col-md-6">
    <button class="btn btn-lg btn-block btn-success" type="submit" name="submit"><i class="icon-check"></i>Add Testimonial</button>
     </div>
     <div class="col-md-6">
    <button class="btn btn-lg btn-block btn-danger" name="reset" type="reset"><i class="icon-refresh"></i>Reset</button>
    </div> 
 </div> </div></div></form>
  
    </div>

 
      
    

