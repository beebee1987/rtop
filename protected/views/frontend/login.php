<html>
  <head>
<title> </title>
<link
href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" /> 
    <link href="<?php echo SITE_URL.'/assets/frontend/css/bootstrap.min.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/font-awesome.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="<?php echo SITE_URL.'/assets/frontend/css/se7en-font.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/isotope.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/jquery.fancybox.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/fullcalendar.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/wizard.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/select2.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/morris.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/datatables.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/datepicker.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/timepicker.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/colorpicker.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/bootstrap-switch.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/daterange-picker.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/typeahead.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/summernote.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/pygments.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/style.css';?>" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/color/green.css';?>" media="all" rel="alternate stylesheet" title="green-theme" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/color/orange.css';?>" media="all" rel="alternate stylesheet" title="orange-theme" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/color/magenta.css';?>" media="all" rel="alternate stylesheet" title="magenta-theme" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/color/gray.css';?>" media="all" rel="alternate stylesheet" title="gray-theme" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/jquery.fileupload-ui.css';?>" media="all" rel="alternate stylesheet" title="gray-theme" type="text/css" />
    <link href="<?php echo SITE_URL.'/assets/frontend/css/ladda-themeless.min.css';?>" media="all" rel="stylesheet" type="text/css" />
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
  </head>
  <div class="page-title">
  </div>  
  <body class="login2">
<div class="login-wrapper">
<img src="<?php echo SITE_URL.'/uploads/logo/'.$getdata['logo'].'?id='.rand(12,19);?>"/>
<?php echo $message; ?>
<form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input class="form-control" placeholder=" Enter Email" type="text" value="<?php echo $email;?>" name="email">
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <input class="form-control" placeholder="Password" type="password" name="password" value="">
          </div>
        </div>
       
        
        <div class="form-group">
        <input class="btn btn-lg btn-primary btn-block" type="submit" value="Login" name="login">
       </div>
         
     </form>
   
   </div>
  
   
   
   
   </body>
   </html>

  <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript">  </script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/bootstrap.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/raphael.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/selectivizr-min.js';?>" type="text/javascript"> </script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.mousewheel.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.vmap.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.vmap.sampledata.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.vmap.world.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.bootstrap.wizard.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/fullcalendar.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/gcal.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.dataTables.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/datatable-editable.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.easy-pie-chart.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/excanvas.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.isotope.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/isotope_extras.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/modernizr.custom.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.fancybox.pack.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/select2.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/styleswitcher.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/wysiwyg.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/summernote.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.inputmask.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.validate.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/bootstrap-fileupload.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/bootstrap-datepicker.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/bootstrap-timepicker.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/bootstrap-colorpicker.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/bootstrap-switch.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/typeahead.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/daterange-picker.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/date.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/spin.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/ladda.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/moment.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/morris.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/skycons.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/fitvids.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/jquery.sparkline.min.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/respond.js';?>" type="text/javascript"></script>
  <script src="<?php echo SITE_URL.'/assets/frontend/js/main.js';?>" type="text/javascript"></script>