<div class="container-fluid main-content">
<div class="page-title">
<h2>Contacts List</h2></div>
 
<div class="row">
 <div class="col-md-12">

<div class="widget-container fluid-height clearfix">
 <div class="widget-content padded">

          
          
  <div class="row">
 <div class="col-md-12">
            <table class="table table-bordered table-striped dataTable" id="dataTable1" >
                  <thead>
                    <tr>
                   
                    <th class="sorting" width="20%">
                    <b>Date</b>
                    </th><th class="sorting" width="40%">
                   <b>E-Mail</b>
                    </th >
                    <th class="sorting" width="40%">
                   <b>Message</b>
                    </th>
                   
                    </tr></thead>
                  
                <tbody>
               
<?php
if(is_array($select))
	foreach ($select as $value)
						{?>
                      <tr>                     
                      <td >
                        <?php $date= $value['created_date'];
                       echo date('d-F-Y', strtotime($date)); ?> 
                      </td>
                     
                      <td>
                        <?php echo $value['contact_email'];?> 
                      </td>
                      <td>
                        <?php echo $value['contact_msg'];?> 
                      </td>
                     
                                      
                    </tr>
                    <?php }?>
                    </tbody>
                  </table>   </div></div> 
                  
          
                  </div></div>
                   
                  </div></div></div>
           
  
  