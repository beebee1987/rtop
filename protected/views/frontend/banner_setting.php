 <div class="container-fluid main-content">
<div class="page-title">
<h2>Change Banner Information</h2></div>
 
 <div class="row">
  <div class="col-md-12">
   <?php echo $display_msg; ?>
<div class="widget-container fluid-height clearfix">
<div class="widget-content padded">

   <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
  <div class="row">
<div class="col-md-12">

<div class="col-md-3"></div>

<div class="col-md-6">
<div class="heading">
<h3>General Information</h3></div><br>
<div class="form-group">
            <label class="control-label col-md-4">Banner Heading</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="banner_heading" value="<?php echo $getdata['banner_heading'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
   <div class="form-group">
            <label class="control-label col-md-4">Banner Text</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="banner_text" value="<?php echo $getdata['banner_text'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
    
        <div class="form-group">
          
            <label class="control-label col-md-4">Upload Banner</label>
            <div class="col-md-5">
              <div class="fileupload fileupload-new" data-provides="fileupload">
              
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                  <?php if(file_exists(SERVER_ROOT.'/uploads/banner/'.$load.'/'.$getdata['banner']) && (($getdata['banner'])!=''))
              { ?>
    <img src="<?php echo SITE_URL.'/uploads/banner/'.$load.'/'.$getdata['banner'];?>" width="100%">
   <?php } else{?>
              	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image"  width="100%">
             <?php } ?>
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px;"></div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Select photo</span>
                  <span class="fileupload-exists">Change</span>
                  <input type="file" name="banner" ></span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
              </div>
            </div>
          </div>
       </div><div class="col-md-3"></div>
     
         
          </div> </div>      
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-5"></div>
            <div class="col-md-3">
             <div class="form-group">
   
           <button class="btn btn-lg btn-block btn-success" type="submit" name="update"><i class="icon-check"></i>Update</button>
                  </div></div></div></div>
                 
           
 </form></div>
 </div></div></div></div>
 




