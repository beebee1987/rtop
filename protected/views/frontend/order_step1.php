<div class="container-fluid main-content">
<div class="row">
<div class="col-md-12">
<?php echo $display_msg1;?>
<div class="col-md-5">
<div class="widget-container fluid-height clearfix">
<div class="widget-content padded">


<?php   if (is_array($get_category))
                    foreach ($get_category as $cat){
    ?><h4>  <?php echo $cat['category'];?></h4>  <?php 
    $get_food=$db->get_all('food',array('category'=>$cat['category']));
                foreach ($get_food as $food) {?> 
<form method="post" class="form" action="" >
             <div class="form-group">
          
<label class="control-label col-md-8"><b><?php echo $food['name']?></b><span class="label label-primary pull-right">
<?php echo $get_setting['currency']."  ".$food['price']?></span></label>
  <div class="col-md-4">
              <div class="input-group">
                <input class="form-control" type="text" name="quantity" placeholder="Qty">
                <input class="form-control" type="hidden" name="food_id" value="<?php echo $food['id'];?>" >
                
                <span class="input-group-btn">
                <button class="btn btn-default" name="add" type="submit"><i class="icon-plus"></i>Add</button></span>
              </div>
            </div>
    </div>
    </form>
    
    <?php }}?>
    
    
</div></div>
</div>

<div class="col-md-7">
<div class="widget-container fluid-height clearfix">
 <div class="widget-content padded">
 <?php echo $display_msg;?>
     <table class="table table-striped invoice-table ">
                    <thead>
                      <tr>
                      <th>Action
                    </th>
                      <th>
                        Name
                      </th>
                      <th class="hidden-xs" >
                       Quantity
                      </th>
                      <th class="hidden-xs">
                        Unit Price
                      </th>
                      <th >
                        Total
                      </th>
                       
                    </tr></thead>
                   <tbody>
               
<?php
if(is_array($select))
	foreach ($select as $value)
						{?>
                      <tr>                     
                      <td>
                       <div class="action-buttons">                                  
                          <a class="table-actions" href="<?php echo $link->link("order_step1",'frontend','&del='.$value['id']);?>"><i class="icon-trash"></i></a>
                        </div>  </td> 
                  
                      <td >
                        <?php echo $value['food_name'];?> 
                      </td>
                      <td class="hidden-xs">
                        <?php echo $value['quantity'];?> 
                      </td>
                      <td class="hidden-xs">
                        <?php echo $get_setting['currency']."  ".$value['price'];?> 
                      </td>  
                      <td >
                        <?php echo $get_setting['currency']."  ".$value['total'];?> 
                      </td>             
                    </tr>
                    <?php }?>
                   
                  </tbody>
                  
                  <tfoot>
                      <tr><?php if($sum==0)
                          {?><td valign="top" colspan="7" class="dataTables_empty">No data Available in Table</td>
                          <?php } else {?>
                        <td class="text-right" colspan="4">
                          <h4 class="text-primary"><b>Total</b></h4>
                        </td>
                        <td><h4 class="text-primary">
                         <b><?php echo $get_setting['currency']."   ".$sum;?></b>
                       </h4> </td><?php }?>
                      </tr>
                     </tfoot>
                  
                  
                     </table>
                
 </div></div>
</div></div></div>

 <form method="post" class="form-horizontal" action="">

  <div class="row">
            <div class="col-md-12">
      
<div class="widget-content padded">
              <div class="well">
                <strong>NOTES</strong>
             <div class="form-group">
             <div class="col-md-12">
           <textarea class="form-control" name="notes" ><?php echo $notes; ?></textarea>
                  </div></div>
           
              </div>
            </div>
          </div></div>
          
    
<div class="row">
<div class="col-md-12">

<div class="col-md-5">

<div class="widget-container fluid-height clearfix">
 <div class="widget-content padded">
<div class="form-group">

            <label class="control-label col-md-3">Select Client</label>
            <div class="col-md-6">
              <select class="form-control" name="client_id">
               <option value="">--------Select Client-----------</option>
                <?php 
                if (is_array($get_client))
                    foreach ($get_client as $client){?>
                    <option value="<?php echo $client['id'];?>" <?php if($client_id==$client['id']){echo "selected";}?>><?php echo $client['name'];?></option>
			<?php } ?>
              
            </select></div>
          
          </div><br>
          
<div class="form-group">

            <label class="control-label col-md-3">Select Table</label>
            <div class="col-md-6">
              <select class="form-control" name="table_id">
               <option value="">--------Select Table-----------</option>
                <?php 
                if (is_array($get_table))
                    foreach ($get_table as $table){?>
                    <option value="<?php echo $table['id'];?>" <?php if($table_id==$table['id']){echo "selected";}?>><?php echo $table['table_no'];?></option>
			<?php } ?>
              
            </select></div>
          
          </div><br>

</div>




</div></div>
<div class="col-md-7">
<div class="widget-container fluid-height clearfix">
 <div class="widget-content padded">

<div class="form-group">

            <label class="control-label col-md-4">Select Payment Method</label>
            <div class="col-md-6">
              <select class="form-control" name="payment_type"  >
               <option value="">--------Select Payment Method-----------</option>
                 <option value="cash" <?php if($payment_type=="cash"){echo "selected";}?>>Cash</option>
                  <option value="credit" <?php if($payment_type=="credit"){echo "selected";}?>>Credit</option>
                   <option value="check" <?php if($payment_type=="check"){echo "selected";}?>>Check</option>
              
            </select></div>
            
          </div><br>




</div>



</div></div>



</div></div>
<div class="row">
<div class="col-md-12">
 <div class="widget-content padded">
<div class="form-group">
<div class="col-md-12">
    <button class="btn btn-lg btn-block btn-success"  name="step2_submit"><i class="icon-check"></i>Submit</button>
   </div> </div></div>

</div>



</div>
</form>

</div>
