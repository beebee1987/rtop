<div class="container-fluid main-content">
<div class="page-title">
<h2>Invoices List</h2></div>
 
<div class="row">
 <div class="col-md-12">
<div class="widget-container fluid-height clearfix">
 <div class="widget-content padded">
 <div class="row">
 <div class="col-md-12">
            <table class="table table-bordered table-striped" id="dataTable1"  >
                  <thead>
                    <tr>
                   <th class="sorting" width="5%">
                    Id
                    </th><th class="hidden-xs" width="10%">
                    Date
                    </th>
                    <th class="hidden-xs" width="30%">
                    Notes
                    </th>
                    <th width="20%">
                    Client Name
                    </th>
                    <th class="hidden-xs" width="15%">
                    Payment Type
                    </th>
                    <th width="10%">
                    Total
                    </th>
                    <th class="sorting" width="20%">Action
                    </th>
                    </tr></thead>
                  
                <tbody>
               
<?php
if(is_array($select))
	foreach ($select as $value)
						{?>
                      <tr>                     
                      <td class="sorting">
                        <?php echo $value['id'];?> 
                      </td>
                      <td class="hidden-xs">
                        <?php $date= $value['created_date'];
                       echo date('d-F-Y', strtotime($date)); ?> 
                      </td>
                     <td class="hidden-xs">
                        <?php echo $value['notes'];?> 
                      </td>
                      <td >
                        <?php $get_client=$db->get_row('user',array('id'=>$value['client_id']));
                         echo $get_client['name'];?> 
                      </td>
                     
                      <td class="hidden-xs">
                        <?php echo $value['payment_type'];?> 
                      </td>
                      <td>
                        <?php echo $value['total_amount'];?> 
                      </td>
                     
                      <td class="action sorting">
                        <div class="action-buttons">                                  
                          <a class="table-actions" href="<?php echo $link->link("order_step2",'frontend','&orderid='.$value['id']);?>"><i class="icon-eye-open"></i></a>
                         
                        </div>
                      </td>                    
                    </tr>
                    <?php }?>
                    </tbody>
                  </table>   </div></div> 
                  
          
                  </div></div>
                   
                  </div></div></div>
           
  