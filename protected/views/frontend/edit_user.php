 <div class="container-fluid main-content">
<div class="row">
<div class="col-md-12">
<h2>Update User<a class="table-actions btn btn-primary pull-right" href="<?php echo $link->link('user',frontend);?>">
<i class="icon-backward"></i>Go Back
 </a> </h2></div></div>
 
 

  
 <div class="row">
  <div class="col-md-12">
   <?php echo $display_msg; ?>
<div class="widget-container fluid-height clearfix">
<div class="widget-content padded">
   <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
  <div class="row">
<div class="col-md-12">
<div class="col-md-2"></div>
<div class="col-md-8">

    
     
    <div class="form-group">
            <label class="control-label col-md-3" >Role</label>
            <div class="col-md-6">
              <div class="input-group">
                <select class="form-control" name="role"  >
               <option value="">----------Select Role--------------</option>
                 <option value="Administrator"   <?php if($getdata['role']=="Administrator"){echo "selected";}?>>Administrator</option>
                  <option value="Client"   <?php if($getdata['role']=="Client"){echo "selected";}?>>Client</option>
		           </select>
               <span class="input-group-addon"  style="color:red">*Required</span></div>  </div>
              </div>
    
    
        
     
    
     <div class="form-group">
            <label class="control-label col-md-3">Name</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="name" value="<?php echo $getdata['name'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
    <div class="form-group">
            <label class="control-label col-md-3">E-Mail</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="email" value="<?php echo $getdata['email'];?>" disabled>
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     
     
     
    
   
      <div class="form-group">
            <label class="control-label col-md-3">Phone No.</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="phone_no" value="<?php echo $getdata['phone_no'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
     
     
      <div class="form-group">
            <label class="control-label col-md-3">Password</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="password" name="pass" value="">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
                
          </div> <div class="col-md-2"></div></div>    </div>   
             <div class="form-group">
             <div class="col-md-12">
              <div class ="col-md-4"></div>
              <div class="col-md-4">
           <button class="btn btn-lg btn-block btn-success" type="submit" name="update"><i class="icon-check"></i>Update</button>
                 </div> </div></div>
                 
           
 </form></div>
 </div></div></div></div>
 


