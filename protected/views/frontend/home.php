 <div class="container-fluid main-content">
      <div class="row">
          <div class="col-md-12">
            <div class="widget-container stats-container">
              <div class="col-md-3">
                <div class="number">
                  <div class="icon-food"></div>
                  <?php echo $get_category;?>
                </div>
                <div class="text">
                <strong>Total Food Category</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="number">
                  <div class="icon-glass"></div>
                  <?php echo $get_food;?>
                </div>
                <div class="text">
                 <strong>Total Food Items</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="number">
                  <div class="icon-home"></div>
                  <?php echo $get_room;?>
                </div>
                <div class="text">
                 <strong>Total Room</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="number">
                  <div class="icon-list"></div>
                  <?php echo $get_table;?>
                </div>
                <div class="text">
                <strong>Total Tables</strong>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="col-md-12">
            <div class="widget-container stats-container">
              <div class="col-md-3">
                <div class="number">
                  <div class="icon-group"></div>
                  <?php echo $get_client;?>
                </div>
                <div class="text">
                <strong>Total Clients</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="number">
                  <div class="icon-dollar"></div>
                  <?php echo $get_invoice;?>
                </div>
                <div class="text">
                 <strong>Total Invoices</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="number">
                  <div class="icon-calendar"></div>
                  <?php echo $get_event;?>
                </div>
                <div class="text">
                 <strong>Total Events</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="number">
                  <div class="icon-user"></div>
                   <?php echo $get_testimonial;?>
                </div>
                <div class="text">
                <strong>Total Testimonial</strong>
                </div>
              </div>
            </div>
         </div>
        </div>
    
         
         
         
    <br>
<div class="page-title">
<h2>Invoices List</h2></div>
 
<div class="row">
 <div class="col-md-12">
<div class="widget-container fluid-height clearfix">
 <div class="widget-content padded">
 <div class="row">
 <div class="col-md-12">
            <table class="table" >
                  <thead>
                    <tr>
                  <th class="hidden-xs">
                    Date
                    </th>
                    <th class="hidden-xs">
                    Notes
                    </th>
                    <th >
                    Client Name
                    </th>
                    <th class="hidden-xs">
                    Payment Type
                    </th>
                    <th >
                    Total
                    </th>
                    <th class="sorting" >Action
                    </th>
                    </tr></thead>
                  
                <tbody>
               
<?php
if(is_array($select))
	foreach ($select as $value)
						{?>
                      <tr>                     
                     
                      <td class="hidden-xs">
                        <?php $date= $value['created_date'];
                       echo date('d-F-Y', strtotime($date)); ?> 
                      </td>
                     <td class="hidden-xs">
                        <?php echo $value['notes'];?> 
                      </td>
                      <td >
                        <?php $get_client=$db->get_row('user',array('id'=>$value['client_id']));
                         echo $get_client['name'];?> 
                      </td>
                     
                      <td class="hidden-xs">
                        <?php echo $value['payment_type'];?> 
                      </td>
                      <td>
                        <?php echo $value['total_amount'];?> 
                      </td>
                     
                      <td class="action sorting">
                        <div class="action-buttons">                                  
                          <a class="table-actions" href="<?php echo $link->link("order_step2",'frontend','&orderid='.$value['id']);?>"><i class="icon-eye-open"></i></a>
                         
                        </div>
                      </td>                    
                    </tr>
                    <?php }?>
                    </tbody>
                  </table>   </div></div> 
                  
          
                  </div></div>
                   
                  </div></div></div>     </div>
           
  
        