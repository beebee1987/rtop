  <div class="container-fluid main-content">
        <div class="page-title">
          <h1>
            Invoice
          </h1>
        </div>
        <div class="invoice">
          <div class="row">
            <div class="col-lg-12">
              <div class="row invoice-header">
                <div class="col-md-6">
                <h4><b><?php echo "Payment Type-   ".$select['payment_type'];?></b></h4>
                </div>
                <div class="col-md-6 text-right">
                  <h2>
                    <?php echo "#".$load;?>
                  </h2>
                  <p>
                   <?php echo date('F d ,Y ');?>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="well">
                <strong>TO</strong>
                <h3>
                <?php echo $get_client['name'];?>
                </h3>
                <p><br>
                 <?php echo $get_client['phone_no'];?> <br><?php echo $get_client['email'];?>
                </p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="well">
                <strong>FROM</strong>
                <h3>
                  <?php echo $get_setting['name'];?>
                </h3>
                <p>
                  <?php echo $get_setting['address'];?><br><?php echo $get_setting['admin_no'];?><br><?php echo $get_setting['email'];?>
                </p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="widget-container fluid-height">
                <div class="widget-content padded clearfix">
                  <table class="table table-striped invoice-table">
                    <thead><tr>
                     
                      <th width="25%">
                        Product
                      </th>
                      <th width="25%">
                        Qty
                      </th>
                      <th width="25%">
                        Unit Price
                      </th>
                      <th width="25%">
                        Total
                      </th><tr>
                    </thead>
                    <tbody>


                    <?php
if(is_array($product_final))
	foreach ($product_final as $value)
						{?>
                      <tr>                     
                      
                  
                      <td >
                        <?php echo $value['food_name'];?> 
                      </td>
                      <td >
                        <?php echo $value['quantity'];?> 
                      </td>
                      <td >
                        <?php echo $get_setting['currency']."   ".$value['price'];?> 
                      </td>  
                      <td >
                        <?php echo $get_setting['currency']."   ".$value['total'];?> 
                      </td>                
                    </tr>
                    <?php }?>
                    </tbody>
                    <tfoot>
                      <tr>
                     <td class="text-right" colspan="3">
                          <h4 class="text-primary">
                            Total
                          </h4>
                        </td>
                     
                        <td>
                          <h4 class="text-primary">
                            <?php echo $get_setting['currency']."   ".$select1['total_amount'];?>
                          </h4>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="well">
                <strong>NOTES</strong>
                <p>
                 <?php echo $select1['notes'];?>
                </p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <a class="btn btn-primary pull-right" onclick="javascript:window.print();"><i class="icon-print"></i>Print invoice</a>
            </div>
          </div>
        </div>
      </div>
   