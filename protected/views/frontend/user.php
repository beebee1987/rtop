<div class="container-fluid main-content">
<div class="page-title">
<h2>Users List</h2></div>
 
<div class="row">
 <div class="col-md-12">

<?php echo $display_msg; ?>
<div class="widget-container fluid-height clearfix">
 <div class="widget-content padded">
 

 <form method="post" class="form-horizontal" action=""  enctype="multipart/form-data">
 <div class="row">
 <div class="col-md-12">
<div class="form-group">
            <label class="control-label col-md-3">Filter users by:</label>
            <div class="col-md-3">
              <select class="form-control" name="role"  >
               <option value="">------------------Select Role--------------------</option>
                 <option value="Administrator"   <?php if($role=="Administrator"){echo "selected";}?>>Administrator</option>
                  <option value="Client"   <?php if($role=="Client"){echo "selected";}?>>Client</option>
		
              
            </select></div><div class="col-md-1">
            <button class="btn   btn-primary" type="submit" name="view"><i class="icon-eye-open"></i>View </button></div>
			<div class="col-md-2"> <a class="btn btn-primary btn fancybox" href="#fancybox-example"><i class="icon-plus"></i>Add New User</a>
			 </div><div class="col-md-3"></div></div>
			
		
          </div>
           </div>
          </form>
          
          
  <div class="row">
 <div class="col-md-12">
            <table class="table table-bordered table-striped" id="dataTable1"  >
                  <thead>
                    <tr>
                   
                    <th class="sorting" width="20%">
                    Role
                    </th><th class=" hidden-xs sorting" width="15%" >
                    Name
                    </th>
                    <th class=" sorting" width="25%">
                    E-Mail
                    </th>
                    <th class="hidden-xs sorting" width="20%">
                    Phone No.
                    </th>
                    <th class="sorting" width="20%">Action
                    </th>
                    </tr></thead>
                  
                <tbody>
               
<?php
if(is_array($select))
	foreach ($select as $value)
						{?>
                      <tr>                     
                      <td class="sorting">
                        <?php echo $value['role'];?> 
                      </td>
                     
                      <td class="hidden-xs">
                        <?php echo $value['name'];?> 
                      </td>
                      <td class="sorting">
                        <?php echo $value['email'];?> 
                      </td>
                      <td class="hidden-xs">
                        <?php echo $value['phone_no'];?> 
                      </td>
                     
                      <td class="action sorting">
                        <div class="action-buttons">                                  
                          <a class="table-actions" href="<?php echo $link->link("edit_user",'frontend','&edit='.$value['id']);?>"><i class="icon-pencil"></i></a>
                          <a class="table-actions" href="<?php echo $link->link("user",'frontend','&del='.$value['id']);?>"><i class="icon-trash"></i></a>
                        </div>
                      </td>                    
                    </tr>
                    <?php }?>
                    </tbody>
                  </table>   </div></div> 
                  
          
                  </div></div>
                   
                  </div></div></div>
           
  
  
            
 
 
 
 
 
 <!--Start  Add User -->
               
 <div id="fancybox-example" style="display: none">
			

 <div class="heading"><h4><b>Add New User</b></h4></div>
   <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
   <div class="row">
  <div class="col-md-12">
     
    <div class="form-group">
            <label class="control-label col-md-3" >Role</label>
            <div class="col-md-6">
              <div class="input-group">
                <select class="form-control" name="role"  >
               <option value="">----------Select Role--------------</option>
                 <option value="Administrator"   <?php if($role=="Administrator"){echo "selected";}?>>Administrator</option>
                  <option value="Client"   <?php if($role=="Client"){echo "selected";}?>>Client</option>
		           </select>
               <span class="input-group-addon"  style="color:red">*Required</span></div>  </div>
              </div>
    
    
     
    
     
    
     <div class="form-group">
            <label class="control-label col-md-3">Name</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="name" value="<?php echo $name;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
      <div class="form-group">
            <label class="control-label col-md-3">E-Mail</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="email" value="<?php echo $email;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
     <div class="form-group">
            <label class="control-label col-md-3">Phone No.</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="phone_no" value="<?php echo $phone_no;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
      <div class="form-group">
            <label class="control-label col-md-3">Password</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="password" name="pass" value="">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
      </div>

  </div>
     <div class="row">
     <div class="col-md-12">
   <div class="form-group">
    <div class="col-md-6">
    <button class="btn btn-lg btn-block btn-success" type="submit" name="submit"><i class="icon-check"></i>Add User</button>
     </div>  <div class="col-md-6">
    <button class="btn btn-lg btn-block btn-danger" name="reset" type="reset"><i class="icon-refresh"></i>Reset</button>
    </div> </div></div></div>


 </form>
 </div>
 
