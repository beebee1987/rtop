<div class="container-fluid main-content">
<div class="page-title">
<h2>Foods List</h2></div>
 
<div class="row">
<div class="col-md-12">
<?php echo $display_msg; ?>
<div class="widget-container fluid-height clearfix">
 <div class="widget-content padded">
 
 <div class="row">
 <div class="col-md-12">

 <div class="col-md-3">
  
  <form method="post" class="form-horizontal" action=""  >
<div class="form-group">
            <label class="control-label col-md-3">Categories:</label>
             <div class="col-md-9">
              <select class="form-control" name="category"  >
               <option value="">-----Select Category------------</option>
                <?php 
                if (is_array($get_category))
                    foreach ($get_category as $category){?>
                    <option value="<?php echo $category['category'];?>"  <?php if($_POST['category']==$category['category']){echo "selected";}?>><?php echo $category['category'];?></option>
			<?php } ?>
              
            </select><br></div>
            <div class="col-md-3"></div><div class="col-md-9">
            <button class="btn   btn-warning" type="submit" name="view"><i class="icon-eye-open"></i>View Foods</button>
			 </div> <div class="col-md-3"></div><div class="col-md-9">
          <a class="btn btn-primary btn fancybox" href="#fancybox-example"><i class="icon-plus"></i>Add a New Food</a>
			</div></div>
			
		</form></div>
		
		
		
		
 <div class="col-md-7">
            <table class="table table-bordered " id="dataTable1"  >
                  <thead>
                    <tr>
                   
                   <th class="sorting" width="20%">
                    Category
                    </th><th width="20%" >
                    Name
                    </th>
                    <th class="hidden-xs" width="30%">
                    Details
                    </th>
                    <th class="hidden-xs" width="20%" >
                    Price
                    </th>
                    <th  class="sorting" width="10%" >Action
                    </th>
                    </tr></thead>
                  
                <tbody>
               
<?php
if(is_array($select))
	foreach ($select as $value)
						{?>
                      <tr>                     
                      
                      
                      <td class="sorting" >
                        <?php echo $value['food_category'];?> 
                      </td>
                      <td >
                        <?php echo $value['name'];?> 
                      </td>
                      <td class="hidden-xs">
                        <?php echo $value['detail'];?> 
                      </td>
                      <td class="hidden-xs">
                        <?php echo $get_setting['currency']."   ".$value['price'];?> 
                      </td>
                     
                      <td class="actions sorting" >
                        <div class="action-buttons">                                  
                          <a class="table-actions" href="<?php echo $link->link("edit_foodlist",'frontend','&edit='.$value['id']);?>"><i class="icon-pencil"></i></a>
                          <a class="table-actions" href="<?php echo $link->link("foodlist",'frontend','&del='.$value['id']);?>"><i class="icon-trash"></i></a>
                        </div>
                      </td>                    
                    </tr>
                    <?php }?>
                    </tbody>
                  </table></div>
                  
          <div class="col-md-2">       
               <h3>Category List</h3>   
                  
                  <table class="table table-bordered">
                  <thead>
                    <tr>
                    
                    <th >
                    Category Name
                    </th>
                    <th >Action
                    </th>
                    </tr></thead>
                  
                <tbody>
               
<?php
if(is_array($select1))
	foreach ($select1 as $value)
						{?>
                      <tr>                     
                      
                   
                      <td >
                        <?php echo $value['category'];?> 
                      </td>
                     
                      <td class="actions ">
                        <div class="action-buttons">                                  
                   <a class="table-actions" href="<?php echo $link->link("foodlist",'frontend','&del_id='.$value['id']);?>"><i class="icon-trash"></i></a>
                        </div>
                      </td>                    
                    </tr>
                    <?php }?>
                    </tbody>
                  </table></div> </div>
                  
                   
                  </div></div></div></div></div>
         
 
 
 <!--Start  Add Food -->
            
  <div id="fancybox-example" style="display: none">
			 
    <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
  
 <div class="row">
  <div class="col-md-12">


 <div class="heading"><h4><b>Food Specification</b></h4></div>
 
    <div class="form-group">
            <label class="control-label col-md-3">Type a Food Category</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="food_category" value="<?php echo $food_category;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     
    <div class="form-group">
            <label class="control-label col-md-3" >Or Choose Category From Existing Ones:</label>
            <div class="col-md-6">
              <div class="input-group">
              <select class="form-control" name="category"  >
               <option value="">--------Select Category--------------------</option>
                <?php $get_category=$db->get_all('category');
                if (is_array($get_category))
                    foreach ($get_category as $category){?>
                    <option value="<?php echo $category['category'];?>" <?php if($_POST['category']==$category['category']){echo "selected";}?> ><?php echo $category['category'];?></option>
			<?php } ?>
              
            </select>
               <span class="input-group-addon"  style="color:red">*Required</span></div>  </div>
           
    </div>
    
     
    
     <div class="form-group">
            <label class="control-label col-md-3">Name</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="name" value="<?php echo $name;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
      <div class="form-group">
            <label class="control-label col-md-3">Details</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="detail" value="<?php echo $detail;?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
      <div class="form-group">
            <label class="control-label col-md-3">Price</label>
            <div class="col-md-6">
             <div class="input-group">
              <input class="form-control" type="text" name="price" value="<?php echo $price;?>">
               <span class="input-group-addon"> <?php echo $get_setting['currency']?></span>
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     <div class="form-group">
            <label class="control-label col-md-3">Tags</label>
            <div class="col-md-6">
          
              <input class="form-control" type="text" name="tags" value="<?php echo $tags;?>">
              
             
             </div>
     </div> 
    
    <div class="form-group">
          
            <label class="control-label col-md-3"> Select Image</label>
            <div class="col-md-4">
              <div class="fileupload fileupload-new" data-provides="fileupload">
             
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                  <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px;"></div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Select photo</span>
                  <span class="fileupload-exists">Change</span>
                  <input type="file" name="image"></span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
              </div>
            </div>
          </div>  
              
            </div>
 </div>
         

 <div class="row">
 <div class="col-md-12">
   <div class="form-group">

<div class="col-md-6">
<button class="btn btn-lg btn-block btn-success" type="submit" name="submit"><i class="icon-check"></i>Add Food</button>
     </div>
     <div class="col-md-6">
<button class="btn btn-lg btn-block btn-danger" name="reset" type="reset"><i class="icon-refresh"></i>Reset</button>
 </div>  </div>  </div> 



   </div>
 </form>
 </div>

</div>
 