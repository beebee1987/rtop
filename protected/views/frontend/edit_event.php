 <div class="container-fluid main-content">
<div class="row">
<div class="col-md-12">
<h2>Update Event<a class="table-actions btn btn-primary pull-right" href="<?php echo $link->link('event',frontend);?>">
<i class="icon-backward"></i>Go Back
 </a> </h2></div></div>
 
 <div class="row">
  <div class="col-md-12">
   <?php echo $display_msg; ?>
<div class="widget-container fluid-height clearfix">
<div class="widget-content padded">
   <form method="post" class="form-horizontal" action="" enctype="multipart/form-data">
  <div class="row">
<div class="col-md-12">
<div class="col-md-6">

   <div class="form-group">
            <label class="control-label col-md-4">Event Name</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="event_name" value="<?php echo $getdata['event_name'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
     <div class="form-group">
            <label class="control-label col-md-4">Post By</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="post_by" value="<?php echo $getdata['post_by'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div> 
    
     <div class="form-group">
            <label class="control-label col-md-4">Event Location</label>
            <div class="col-md-7">
             <div class="input-group">
              <input class="form-control" type="text" name="event_loc" value="<?php echo $getdata['event_loc'];?>">
              <span class="input-group-addon"  style="color:red">*Required</span></div>
             </div>
     </div>
     
   <div class="form-group">
            <label class="control-label col-md-4">Date and Time</label>
            <div class="col-md-3">
            <div class="input-group date datepicker" data-date-autoclose="true" data-date-format="dd-mm-yyyy">
             <input class="form-control" type="text" name="date" value="<?php echo $getdata['date'];?>"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              </div> </div>
               <div class="col-md-4">
              <div class="input-group bootstrap-timepicker"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td>
              <a href="" data-action="incrementHour"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td>
              <a href="" data-action="incrementMinute"><i class="fa fa-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td class="meridian-column">
              <a href="" data-action="toggleMeridian"><i class="fa fa-chevron-up"></i></a></td></tr><tr><td>
              <input type="text" name="hour" class="bootstrap-timepicker-hour form-control" maxlength="2"></td> 
              <td class="separator">:</td><td>
              <input type="text" name="minute" class="bootstrap-timepicker-minute form-control" maxlength="2"></td> <td class="separator">&nbsp;</td><td>
              <input type="text" name="meridian" class="bootstrap-timepicker-meridian form-control" maxlength="2"></td></tr><tr><td>
              <a href="" data-action="decrementHour"><i class="fa fa-chevron-down"></i></a></td><td class="separator"></td><td>
              <a href="" data-action="decrementMinute"><i class="fa fa-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td>
              <a href="" data-action="toggleMeridian"><i class="fa fa-chevron-down"></i></a></td></tr></tbody></table></div>
                <input class="form-control" id="timepicker-default" name="time" type="text" value="<?php echo $getdata['time'];?>"><span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
              </div>
            </div>
       </div>
       
    <div class="form-group">
          <label class="control-label col-md-4">Event Description</label>
          <div class="col-md-7">
          <div class="input-group">
           <textarea class="form-control" name="event_des" ><?php echo $getdata['event_des']; ?></textarea>
                  <span class="input-group-addon"  style="color:red">*Required</span></div>
            </div> </div> </div>
     <div class="col-md-6">
       <div class="form-group">
          
            <label class="control-label col-md-3">Select Image</label>
            <div class="col-md-5">
              <div class="fileupload fileupload-new" data-provides="fileupload">
              
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                  <?php if(file_exists(SERVER_ROOT.'/uploads/event/'.$load.'/'.$getdata['image']) && (($getdata['image'])!=''))
              { ?>
    <img src="<?php echo SITE_URL.'/uploads/event/'.$load.'/'.$getdata['image'];?>" width="100%">
   <?php } else{?>
              	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image"  width="100%">
             <?php } ?>
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px;"></div>
                <div>
                  <span class="btn btn-default btn-file"><span class="fileupload-new">Select photo</span>
                  <span class="fileupload-exists">Change</span>
                  <input type="file" name="image" ></span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
              </div>
            </div>
          </div>
                     
                  
          </div> </div>    </div> 
           <div class="row">
            <div class="col-md-12">  
             <div class="form-group">
              <div class="col-md-12"> 
           <button class="btn btn-lg btn-block btn-success" type="submit" name="update"><i class="icon-check"></i>Update</button>
                  </div></div></div></div>
                 
           
 </form></div>
 </div></div></div></div>
 


