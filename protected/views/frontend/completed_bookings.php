<div class="container-fluid main-content">
<div class="page-title">
<h2>Completed Bookings List</h2></div>

<div class="row">
 <div class="col-md-12">
 <?php echo $display_msg;?>
<div class="widget-container fluid-height clearfix">
 <div class="widget-content padded">
 <div class="row">
 <div class="col-md-12">
            <table class="table table-bordered table-striped" id="dataTable1"  >
                  <thead>
                    <tr>
                   <th class="sorting" width="15%">
                    Name
                    </th><th class="hidden-xs" width="10%">
                    No. of Persons
                    </th>
                    <th class="hidden-xs" width="20%">
                    E-Mail
                    </th>
                    <th class ="hidden-xs" width="15%">
                    Phone No.
                    </th>
                    <th  width="15%">
                   Date
                    </th>
                    <th width="10%">
                    Time
                    </th>
                     <th class="sorting" width="15%">
                   Action
                    </th>
                    
                    </tr></thead>
                  
                <tbody>
               
<?php
if(is_array($select))
	foreach ($select as $value)
						{?>
                      <tr>                     
                      <td class="sorting">
                        <?php echo $value['name'];?> 
                      </td>
                      <td class="hidden-xs">
                        <?php  echo $value['person']; ?> 
                      </td>
                     <td class="hidden-xs">
                        <?php echo $value['email'];?> 
                      </td>
                      <td class ="hidden-xs">
                        <?php echo $value['phone_no'];?> 
                      </td>
                     
                      <td >
                        <?php echo $value['date'];?> 
                      </td>
                      <td>
                        <?php echo $value['time'];?> 
                      </td>
                     
                      <td class="action sorting">
                        <div class="action-buttons">                                  
                           <a class="table-actions" href="<?php echo $link->link("completed_bookings",'frontend','&del='.$value['id']);?>"><i class="icon-trash"></i></a>
                           
<?php if($value['guests']==0){?><a class="btn btn-xs btn-primary" 
href="<?php echo $link->link("completed_bookings",'frontend','&guest='.$value['id']);?>">Mark Guests Arrived</a>
<?php }
else 
{
?>
<a class="btn btn-xs btn-default" 
href="#">Passed</a>
<?php 
}

?>                         
                        </div>
                      </td>                    
                    </tr>
                    <?php }?>
                    </tbody>
                  </table>   </div></div> 
                  
          
                  </div></div>
                   
                  </div></div></div>
           
  