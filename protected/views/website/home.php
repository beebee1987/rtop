<div class="flexslider">
			<ul class="slides">
				<li>
					<img src="<?php echo SITE_URL.'/uploads/banner/'.$get_banner['banner'];?>" alt="Slider" />
					<div class="container">
						
						  <div class="row">
						  <div class="col-md-1"></div>
							<div class="col-md-10 centered">
							  <h1><?php echo $get_banner['banner_heading'];?></h1>
							  <h2><?php echo $get_banner['banner_text'];?></h2>
							</div>
						  <div class="col-md-1"></div>
						  </div>
						
				  </div>
				</li>
				
			</ul></div>
	
		
	<div class="col-md-12">
 <form method="post" class="form-horizontal" action="">
  <div class="col-md-12">
  <?php echo $display_msg;?>
 <div class="col-md-2">
 <div class="form-group">
 <label class="control-label col-md-4">Name</label> 
  <div class="col-md-12">
 <input class="form-control" type="text" name="name" value="<?php echo $name;?>"></div></div></div>
 
 
 <div class="col-md-1">
   <div class="form-group">    
 <label class="control-label col-md-5">Persons</label>
<div class="col-md-12">
 <select class="form-control" name="person">
 <option value="">NO.</option>
 <option value="1" <?php if($person=="1"){echo "selected";}?>> 1 </option>
 <option value="2" <?php if($person=="2"){echo "selected";}?>> 2 </option>
 <option value="3" <?php if($person=="3"){echo "selected";}?>> 3 </option>
 <option value="4" <?php if($person=="4"){echo "selected";}?>> 4 </option>
 <option value="5" <?php if($person=="5"){echo "selected";}?>> 5 </option>
 <option value="6" <?php if($person=="6"){echo "selected";}?>> 6 </option>
 <option value="7" <?php if($person=="7"){echo "selected";}?>> 7 </option>
 <option value="8" <?php if($person=="8"){echo "selected";}?>> 8 </option>
 <option value="9" <?php if($person=="9"){echo "selected";}?>> 9 </option>
 <option value="10" <?php if($person=="10"){echo "selected";}?>> 10 </option>

    
  </select> </div></div></div>
      
<div class="col-md-2">
 <div class="form-group">
 <label class="control-label col-md-4">Date</label> 
  <div class="col-md-12">
 <input class="form-control" type="text" name="date" placeholder="mm-dd-yy" value="<?php echo $date;?>"></div></div>
 </div>
 <div class="col-md-1">
 <div class="form-group">
 <label class="control-label col-md-7">Time</label> 
  <div class="col-md-12">
 <input class="form-control" type="text" name="time" placeholder="hh:mm" value="<?php echo $time;?>"></div></div>
 </div>
  <div class="col-md-3">
 <div class="form-group">
 <label class="control-label col-md-4">E-Mail</label> 
  <div class="col-md-12">
 <input class="form-control" type="text" name="email" value="<?php echo $email;?>"></div></div>
 </div>
 <div class="col-md-2">
 <div class="form-group">
 <label class="control-label col-md-4">Phone</label> 
  <div class="col-md-12">
 <input class="form-control" type="text" name="phone_no" value="<?php echo $phone_no;?>"></div></div>
 </div>
              
 <div class="col-md-1 contact">
 <div class="form-group">
 <label class="control-label col-md-1"><br></label> 
  <div class="col-md-12">

  <input type="submit" name="submit" value="Book Table" class="btn btn-primary" />
 </div></div>
 </div>
 	  </div>
      
            
  </form> </div>	
  
    
   
    
    
    
    
    
  <div class="container">

			
			<div class="row" id="s0"> <br><br>
			<h2>Forthcoming <strong>Events</strong></h2>
				<?php
                if(is_array($event))
	           foreach ($event as $event_image)
						{?>
				
				<div class="col-md-3 col-sm-6">
			<div class="dealimg">
			<img src="<?php echo SITE_URL.'/uploads/event/'.$event_image['id'].'/'.$event_image['image']; ?>" alt="Hot Deal" class="img-responsive center-block" />
			</div>
			<p>
			<strong>Event : </strong><?php echo $event_image['event_name'];?><br>
            <strong>Posted By : </strong> <?php echo $event_image['post_by']?><br>
            <strong> Date : </strong> <?php echo $event_image['date'];?><br>
             <strong> Time : </strong> <?php echo $event_image['time'];?><br>
             <strong> Location : </strong> <?php echo $event_image['event_loc'];?><br>
             <strong>Description : </strong>  <?php echo $event_image['event_des']; ?>
				
					
					</p>
				</div>
				<?php }?>
				
				
				
			</div>
			
			
			<div class="row nomgbtm section s1" id="s1">
				<div class="col-md-12">
			<h2>ORDER ONLINE / <strong>OUR MENU</strong> / <a href="<?php echo $link->link('menu',website);?>">OPEN FULL PAGE MENU</a></h2>
				</div>
			</div>
				<div class="accordion">
				
					<?php
                if(is_array($category))
	           foreach ($category as $category_name)
						{?>
				
				
					<h3><span class="menutitle">MENU / <strong><?php echo $category_name['category'];?></strong></span></h3>
					
					<div class="section">
					<?php 
					$count=1;
						 $get_food=$db->get_all('food',array('category'=>$category_name['category']));
						 if(is_array($get_food))
	                        foreach ($get_food as $food) { 
						if($count%2!=0)
						    echo '<div class="row">';
						?>
							<div class="col-md-6">
								<div class="item simpleCart_shelfItem">
									<div class="info">
										<div class="dots"></div>
										<span class="name item_name"><?php echo $food['name'];?></span>
					<span class="price item_price"><?php echo $setting['currency']." ".$food['price'];?></span>
									</div>
					<div class="description"><?php echo $food['detail'];?>
						<div class=" pull-right">
									
		<?php if(file_exists(SERVER_ROOT.'/uploads/food/'.$food['id'].'/'.$food['image']) && (($food['image'])!=''))
     { ?>
     <a href="<?php echo SITE_URL.'/uploads/food/'.$food['id'].'/'.$food['image'];?>" data-lightbox="thegallery" title="<?php echo $food['name'];?>">
<img src="<?php echo SITE_URL.'/uploads/food/'.$food['id'].'/'.$food['image'];?>" width="80" height="80">
 <?php } else{?>
     <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image"  width="80" height="80"></a>
   <?php } ?>
								</div>
						
						</div>
									
									<div class="legend">
									<?php 
									$tagsarray= explode(',', $food['tags']);
									foreach ($tagsarray as $tag)	
									{							
									if($tag!='')
									echo '<span>'.$tag.'</span>';
									}
									if($setting['enable']=='yes')
									{?>
									<a class="add item_add" href="javascript:;">
									ADD
									</a>
									<?php }?>
									
									</div>
									
									<input type="hidden" value="1" class="item_Quantity">
								</div>
							</div>
							
							
						<?php 
						if($count%2==0 || count($get_food)==$count)
						{
						  	
						echo '</div>';
						}
$count++;
						    }
						?>
										
					
					
						
					</div>

				<?php }?>
</div>

	


			<?php if($setting['enable']=='yes')
									{?>
			
			<div class="row orderbtn">
				<div class="col-md-12 text-center">
					<a class="placeorder button green">REVIEW ORDER</a>
				</div>
			</div>
			
			<?php }?>
			<div class="row team section s2" id="s2">
			             <h2>TESTIMONIAL / <strong>OUR CLIENTS</strong></h2>
				<?php
                if(is_array($get_testimonial))
	           foreach ($get_testimonial as $testimonial)
						{?>
				
				<div class="col-md-3 col-sm-6 portrait text-center">
					<img   class="img-responsive center-block" src="<?php echo SITE_URL.'/uploads/testimonial/'.$testimonial['id'].'/'.$testimonial['image']; ?>"  alt="Chef's Name"/>
					<h4><?php echo $testimonial['name'];?></h4>
				
				<p><strong>Job : </strong><?php echo $testimonial['job'];?>
				<strong>At </strong><?php echo $testimonial['work_at'];?><br>
               <strong>Content :</strong><?php echo $testimonial['content'];?></p>
            
					
							
							
							</div>
			
				<?php }?>
			</div>
			
	
			<div class="row puretext section s3" id="s3">
				<div class="col-md-12">
					<h2>A FEW WORDS / <strong>ABOUT OUR RESTAURANT</strong></h2>
					<p><?php echo html_entity_decode($about['notes']);?></p>
				</div>
			</div>
			
			
			<div class="row section s4" id="s4">
				<h2>RESTAURANTE / <strong>PHOTO GALLERY</strong></h2>
				<div class="gallery">
					<?php
                if(is_array($gallery))
	           foreach ($gallery as $gallery_name)
						{?>
					<div class="slide">
<a href="<?php echo SITE_URL.'/uploads/gallery/'.$gallery_name['image_category'].'/'.$gallery_name['image'];?>"
	 data-lightbox="thegallery" 
	title="<?php echo $gallery_name['image'];?>">
<img src="<?php echo SITE_URL.'/uploads/gallery/'.$gallery_name['image_category'].'/'.$gallery_name['image'];?>"></a>
	</div>
	<?php }?>
					
					
					
						</div>
			</div>

		
		
		
		</div>

		<div class="section s6" id="s6"></div>
		<footer>
			<div class="fullspan yellow">
				<section id="parallax" class="homeSlide">
					<div class="bcg" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -100px;" data-anchor-target="#parallax">
						<div class="container">
							<div class="row">
								<div class="col-md-12 quote">
									<h3>&#8220;  Come dine with us. We will blow your senses away! &#8221;</h3>
								</div>
							</div>
						</div>						
					</div>
				</section>			
			</div>

			
			
			<div class="fullspan dark">
				<div class="container">
				
					<div class="row">
						<div class="col-md-4 address">
							<h4><i class="fa fa-heart"></i><?php echo $setting['name'];?></h4>
							<p><strong><span><?php echo $setting['address'];?><br><?php echo $setting['phone_no'];?></span></strong>
							<br>
							<span class="email"><a href="mailto:restaurante@restaurante.com"><?php echo $setting['email'];?></a></span></p>
							
							
							
							<ul class="social">
								<li class="twitter"><a class="symbol" href="<?php echo $social['twitter'];?>">twitterbird</a></li>
								<li class="facebook"><a class="symbol" href="<?php echo $social['facebook'];?>">facebook</a></li>
								<li class="google"><a class="symbol" href="<?php echo $social['google'];?>">googleplus</a></li>
								<li class="pinterest"><a class="symbol" href="<?php echo $social['pinterest'];?>">pinterest</a></li>
								<li class="linkedin"><a class="symbol" href="<?php echo $social['linkedin'];?>">linkedin</a></li>
							</ul>
				
						</div>
						
						<div class="col-md-4 hours">
							<h4><i class="fa fa-clock-o"></i>OPENING HOURS</h4>
							<p>
	<?php if($day=='Mon'){?><span style="color:red"><strong>Monday:</strong><?php echo $setting['mon'];?><br></span><?php }else {?>
	<strong>Monday:</strong><?php echo $setting['mon'];?><br><?php }?>
	<?php if($day=='Tue'){?><span style="color:red"><strong>Tuesday:</strong><?php echo $setting['tues'];?></span><?php }else {?>
	 <strong>Tuesday:</strong><?php echo $setting['tues'];?><br><?php }?>
	<?php if($day=='Wed'){?><span style="color:red"> <strong>Wednesday:</strong><?php echo $setting['wed'];?><br></span><?php } else{?>
	<strong>Wednesday:</strong><?php echo $setting['wed'];?><br><?php }?>
	<?php if($day=='Thu'){?><span style="color:red"> <strong>Thursday:</strong><?php echo $setting['thurs'];?><br></span><?php }else {?>
	<strong>Thursday:</strong><?php echo $setting['thurs'];?><br><?php }?>
	<?php if($day=='Fri'){?><span style="color:red"> <strong>Friday:</strong><?php echo $setting['fri'];?><br></span><?php }else {?>
	 <strong>Friday:</strong><?php echo $setting['fri'];?><br><?php }?>
	<?php if($day=='Sat'){?><span style="color:red"> <strong>Saturday:</strong><?php echo $setting['sat'];?><br></span><?php }else {?>
	<strong>Saturday:</strong><?php echo $setting['sat'];?><br><?php }?>
	<?php if($day=='Sun'){?><span style="color:red"> <strong>Sunday:</strong><?php echo $setting['sun'];?><br></span><?php }else{?>
	<strong>Sunday:</strong><?php echo $setting['sun'];?><br><?php }?>
							</p>
						
						</div><!-- end .col -->
						
						<div class="col-md-4 contact">
							<h4><i class="fa fa-phone"></i>CONTACT FORM</h4>
							<?php echo $message;?>
							<form action="#s6" method="post"  class="contactform">
								<input type="text" name="contact_email"  class="input" value="<?php echo $contact_email;?>" placeholder="Enter your email" />
								<textarea name="contact_msg"  class="textarea"><?php echo $contact_msg;?></textarea>
								<input type="submit" name="sendmessage"  class="submit sendmessage" value="SEND MESSAGE" />
							</form>
							
							
						</div><!-- end .col -->
					</div>

				</div><!-- end .container -->
			</div><!-- end .fullspan -->
			<div class="fullspan copy">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
				<?php echo date('Y');?> &copy;  <a href="<?php echo SITE_URL;?>" ><?php echo $setting['name'];?></a> All rights reserved. By  <a href="http://www.iwcnetwork.com" target="_blank" >IWCN</a>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<div class="modal fade">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Your order</h4>
			  </div>
			  <div class="modal-body">
						<div class="simpleCart_items"></div>
						<span class="simpleCart_quantity"></span> items - <span class="simpleCart_total"></span>
						
						<ul class="deliveryaddress">
							<li><label>House Number</label><input type="text" name="houseno" id="houseno" /></li>
							<li><label>Street</label><input type="text" name="street" id="street" /></li>
							<li><label>City</label><input type="text" name="city" id="city" /></li>
							<li><label>Postcode</label><input type="text" name="postcode" id="postcode" /></li>
							<li><label>Phone</label><input type="text" name="phone" id="phone" /></li>
						</ul>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Add more</button>
				<a href="javascript:;" class="btn btn-primary simpleCart_checkout">Checkout</a>
			  </div>

			</div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->