<!DOCTYPE html>
<html lang="en" class="no-js">
<?php $setting=$db->get_row('setting',array('id'=>1)); ?>
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title><?php echo $setting['name'];?></title>
		<meta name="description" content="<?php echo $setting['description'];?>"/>
		
			<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL.'/assets/website/css/jquery.jscrollpane.custom.css';?>" />
		<link rel="stylesheet" href="<?php echo SITE_URL.'/assets/website/css/bootstrap.min.css';?>">
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL.'/assets/website/css/bookblock.css';?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL.'/assets/website/css/custom.css';?>" />
		<link rel="stylesheet" href="<?php echo SITE_URL.'/assets/website/css/styles.css';?>">
		<link rel="stylesheet" href="<?php echo SITE_URL.'/assets/website/css/lightbox.css';?>">
				
		
		<script src="<?php echo SITE_URL.'/assets/website/js/modernizr.custom.79639.js';?>"></script>
        <script src="<?php echo SITE_URL.'/assets/website/js/jquery-2.1.0.min.js';?>"></script>
		<script src="<?php echo SITE_URL.'/assets/website/js/bootstrap.min.js';?>"></script>
		<script src="<?php echo SITE_URL.'/assets/website/js/simpleCart.min.js';?>"></script>
		<script src="<?php echo SITE_URL.'/assets/website/js/lightbox-2.6.min.js';?>"></script>

		<script>
		$(document).ready(function(){

			$('.placeorder').click(function(){
				$('.modal').modal({backdrop:true,});
			});
		});
		</script>
		<script>
		simpleCart({
			checkout: {
				type: "PayPal",
				email: "<?php echo $setting['paypal_email'];?>"
			},
			 cartColumns: [
				{ attr: "name" , label: "Name" } ,
				{ attr: "price" , label: "Price", view: 'currency' } ,
				{ view: "decrement" , label: "&nbsp;" , text: "-" } ,
				{ attr: "quantity" , label: "Qty" } ,
				{ view: "increment" , label: "&nbsp;" , text: "+" } ,
				{ attr: "total" , label: "SubTotal", view: 'currency' } ,
				{ view: "remove" , text: "Remove" , label: "&nbsp;" }
			],
		
		});
		// simple callback example
		simpleCart.bind( 'beforeAdd' , function( item ){
			//alert("modal");
			$('.modal').modal({
				backdrop:	true,
			});
		});
		</script>		

		
	</head>
	<body>
		<div id="container" class="container">	

			<div class="menu-panel">
		
			
				<h3><a href="<?php echo $link->link('home',website);?>"><img src="<?php echo SITE_URL.'/uploads/logo/'.$setting['logo'];?>"  />
				</a><br>MENU
				
				</h3>
			
				<ul id="menu-toc" class="menu-toc">
		     <?php
		         $c=0;
               if(is_array($get_category))
	           foreach ($get_category as $category_name)
			   {?>
				
				<li <?php 
				if($c==0)
				{
				?>class="menu-toc-current" <?php } else echo 'class';?>>
				<a href="#item<?php echo $c;?>"><?php echo $category_name['category'];?></a></li>
					<?php $c++; }
				if($setting['enable']=='yes')
									{?>
			<li><a href="#" class="placeorder">YOUR ORDER</a></li>
			<?php }?>
				</ul>
			</div>
		
		<div class="bb-custom-wrapper">
				<div id="bb-bookblock" class="bb-bookblock">
					
					
						<?php 
						$c=0;
						if(is_array($get_category))
						    foreach ($get_category as $category_name)
									   {?>
									   
									   <div class="bb-item" id="item<?php echo $c;?>" >
						<div class="content">
							<div class="scroller">
                           	
								<h3>MENU / <strong><?php echo $category_name['category'];?></strong></h3>
								
								<div class="section">
								<?php 
						 $get_food=$db->get_all('food',array('category'=>$category_name['category']));
						 $count=1;
						 if(is_array($get_food))
	                        foreach ($get_food as $food) { 
						
						   if($count%2!=0)
						    echo '<div class="row">';
					               	?>
										<div class="col-md-6">
											<div class="item simpleCart_shelfItem">

												<div class="info">
													
													
													<div class="dots"></div>
													
													<span class="name item_name"><?php echo $food['name'];?></span>
													
													<span class="price item_price"><?php echo $setting['currency']." ".$food['price'];?></span>
												
												</div>
                        <div class="description"><?php echo $food['detail'];?>
						<div class=" pull-right">
									
		<?php if(file_exists(SERVER_ROOT.'/uploads/food/'.$food['id'].'/'.$food['image']) && (($food['image'])!=''))
     { ?>
     <a href="<?php echo SITE_URL.'/uploads/food/'.$food['id'].'/'.$food['image'];?>" data-lightbox="thegallery" title="<?php echo $food['name'];?>">
<img src="<?php echo SITE_URL.'/uploads/food/'.$food['id'].'/'.$food['image'];?>" width="80" height="80">
 <?php } else{?>
     <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image"  width="80" height="80"></a>
   <?php } ?>
								</div>
						
						
						
						
						</div>
												
												<div class="legend">
												<?php 
									$tagsarray= explode(',', $food['tags']);
									foreach ($tagsarray as $tag)	
									{							
									if($tag!='')
									echo '<span>'.$tag.'</span>';
									} if($setting['enable']=='yes')
									{?>
												<a class="add item_add" href="javascript:;">ADD</a>
									<?php }?>		</div>	
												<input type="hidden" value="1" class="item_Quantity">
											</div>	
										</div>
										
										<?php 
						if($count%2==0 || count($get_food)==$count)
						{
						  	
						echo '</div>';
						}
$count++;                   
						    
						?>
						<?php }?>
						 </div><!-- end .section -->	
							</div>
							
							</div>
						</div> 
                    <?php $c++; }?>
				</div>
				
				<nav>
					<span id="bb-nav-prev">&larr;</span>
					<span id="bb-nav-next">&rarr;</span>
						
				</nav>
<span id="bb-nav-next">&rarr;</span>
				<span id="tblcontents" class="menu-button">Table of Contents</span>

			</div>
				
		</div><!-- /container -->

		<script src="<?php echo SITE_URL.'/assets/website/js/jquery.mousewheel.js';?>'"></script>
		<script src="<?php echo SITE_URL.'/assets/website/js/jquery.jscrollpane.min.js';?>"></script>
		<script src="<?php echo SITE_URL.'/assets/website/js/jquerypp.custom.js';?>"></script>
		<script src="<?php echo SITE_URL.'/assets/website/js/jquery.bookblock.js';?>"></script>
		<script src="<?php echo SITE_URL.'/assets/website/js/page.js';?>"></script>
		<script>
			$(function() {

				Page.init();

			});
		</script>

		
		<div class="modal fade">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Your order</h4>
			  </div>
			  <div class="modal-body">
						<div class="simpleCart_items"></div>
						<span class="simpleCart_quantity"></span> items - <span class="simpleCart_total"></span>
						
						<ul class="deliveryaddress">
							<li><label>House Number</label><input type="text" name="houseno" id="houseno" /></li>
							<li><label>Street</label><input type="text" name="street" id="street" /></li>
							<li><label>City</label><input type="text" name="city" id="city" /></li>
							<li><label>Postcode</label><input type="text" name="postcode" id="postcode" /></li>
							<li><label>Phone</label><input type="text" name="phone" id="phone" /></li>
						</ul>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Add more</button>
				<a href="javascript:;" class="btn btn-primary simpleCart_checkout">Checkout</a>
			  </div>

			</div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->		
		
		
		</body>
</html>
