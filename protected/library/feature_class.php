<?php
class feature {
	private $address = null;
	/**
	 *
	 * @param
	 *       	 type 0 return first character of string in capital letter.
	 *       	
	 * @param
	 *       	 type 1 return first character of all word in capital letter.
	 *       	
	 * @param
	 *       	 type 2 return all character of string in small letters.
	 *       	
	 * @param
	 *       	 type 3 return all character of string in capital letters.
	 *       	
	 * @param
	 *       	 type 4 return first character of string in small letter.
	 *       	
	 * @param
	 *       	 type 5 return first character of all word in capital letter
	 *       	 (also convert other characters in small).
	 *       	
	 */
	public function textstyler($string, $type) {
		
		$str = trim ( preg_replace ( '/[^A-Za-z0-9\-]/', ' ', $string ) ); // Removes
		                                                                   // special
		                                                                   // chars.
		
		if ($type == 0) {
			return ucfirst ( $str );
		} elseif ($type == 1) {
			return ucwords ( $str );
		} elseif ($type == 2) {
			return strtolower ( $str );
		} elseif ($type == 3) {
			return strtoupper ( $str );
		} elseif ($type == 4) {
			return lcfirst ( $str );
		} elseif ($type == 5) {
			return ucwords ( strtolower ( $str ) );
		}
	}
	/**
	 *
	 * @param $string is
	 *       	 string
	 * @param $replacer is
	 *       	 the replace character which replace space from string.
	 */
	public function space_replacer($string, $replacer) {
		return str_replace ( ' ', $replacer, $string );
	}
	/**
	 *
	 * @param $string is
	 *       	 the string
	 * @return where new line in your string it replace with spaces string
	 *         return
	 */
	public function remove_newline($string) {
		return trim ( preg_replace ( '/\s+/', ' ', $string ) );
	}
	// distance calculator
	function getDistanceFromLatLonInKm($lat1, $lon1, $lat2, $lon2) {
		$R = 6371; // Radius of the earth in km
		$dLat = deg2rad ( $lat2 - $lat1 ); // deg2rad below
		$dLon = deg2rad ( $lon2 - $lon1 );
		$a = sin ( $dLat / 2 ) * sin ( $dLat / 2 ) + cos ( deg2rad ( $lat1 ) ) * cos ( deg2rad ( $lat2 ) ) * sin ( $dLon / 2 ) * sin ( $dLon / 2 );
		$c = 2 * (atan2 ( sqrt ( $a ), sqrt ( 1 - $a ) ));
		$distance = $R * $c; // Distance in km
		return $distance;
	}
	// get areas less than given kilometers and returns array
	function getcoveredareas_array($lat1, $lon1, $deliveryareasarray = array(), $kms) {
		foreach ( $deliveryareasarray as $key => $val ) {
			$dis = $this->getDistanceFromLatLonInKm ( $lat1, $lon1, $val ['lat'], $val ['lng'] );
			if ((($dis * .17) + $dis) <= $kms) {
				$getselected [] = $val ['id'];
				$getregion [] = $val ['region'];
			}
		}
		return (array_combine ( $getselected, $getregion ));
	}
	
	function getOS() {
	
		$user_agent=$_SERVER['HTTP_USER_AGENT'];
	
		$os_platform    =   "Unknown OS Platform";
	
		$os_array       =   array(
				'/windows nt 6.2/i'     =>  'Windows 8',
				'/windows nt 6.1/i'     =>  'Windows 7',
				'/windows nt 6.0/i'     =>  'Windows Vista',
				'/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
				'/windows nt 5.1/i'     =>  'Windows XP',
				'/windows xp/i'         =>  'Windows XP',
				'/windows nt 5.0/i'     =>  'Windows 2000',
				'/windows me/i'         =>  'Windows ME',
				'/win98/i'              =>  'Windows 98',
				'/win95/i'              =>  'Windows 95',
				'/win16/i'              =>  'Windows 3.11',
				'/macintosh|mac os x/i' =>  'Mac OS X',
				'/mac_powerpc/i'        =>  'Mac OS 9',
				'/linux/i'              =>  'Linux',
				'/ubuntu/i'             =>  'Ubuntu',
				'/iphone/i'             =>  'iPhone',
				'/ipod/i'               =>  'iPod',
				'/ipad/i'               =>  'iPad',
				'/android/i'            =>  'Android',
				'/blackberry/i'         =>  'BlackBerry',
				'/webos/i'              =>  'Mobile'
		);
	
		foreach ($os_array as $regex => $value) {
	
			if (preg_match($regex, $user_agent)) {
				$os_platform    =   $value;
			}
	
		}
	
		return $os_platform;
	
	}
	/* get all time zones in an array
	 * */
	function get_timezones() {
		$zones_array = array();
		$timestamp = time();
		foreach(timezone_identifiers_list() as $key => $zone) {
			date_default_timezone_set($zone);
			$zones_array[$key]['zone'] = $zone;
			$zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
		}
		return $zones_array;
	}
	
	function getBrowser() {
	
		$user_agent=$_SERVER['HTTP_USER_AGENT'];
	
		$browser        =   "Unknown Browser";
	
		$browser_array  =   array(
				'/msie/i'       =>  'IE',
				'/firefox/i'    =>  'Firefox',
				'/safari/i'     =>  'Safari',
				'/chrome/i'     =>  'Chrome',
				'/opera/i'      =>  'Opera',
				'/netscape/i'   =>  'Netscape',
				'/maxthon/i'    =>  'Maxthon',
				'/konqueror/i'  =>  'Konqueror',
				'/mobile/i'     =>  'Handheld Browser'
		);
	
		foreach ($browser_array as $regex => $value) {
	
			if (preg_match($regex, $user_agent)) {
				$browser    =   $value;
			}
	
		}
	
		return $browser;
	
	}
	
	
	function createDateRangeArray($strDateFrom,$strDateTo)
	{
		// takes two dates formatted as YYYY-MM-DD and creates an
		// inclusive array of the dates between the from and to dates.
	
		// could test validity of dates here but I'm already doing
		// that in the main script
	
		$aryRange=array();
	
		$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));
	
		if ($iDateTo>=$iDateFrom)
		{
			array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
			while ($iDateFrom<$iDateTo)
			{
				$iDateFrom+=86400; // add 24 hours
				array_push($aryRange,date('Y-m-d',$iDateFrom));
			}
		}
		return $aryRange;
	}
	public function rrmdir($dir) {
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != ".." ){
					if (filetype($dir."/".$object) == "dir")
							self::rrmdir($dir."/".$object);
					else unlink   ($dir."/".$object);
				}
			}
			reset($objects);
			rmdir($dir);
		}
	}
	
}
?>