<?php 
class mandrill_mail 
{

// Mandrill Mail //
public function mandrill($to_email,$to_name,$subject,$html,$type)
{
	require_once SERVER_ROOT.'/protected/external-mail/mandrilla/Mandrill.php';
	try {
		$mandrill = new Mandrill(Mandrill_Api_Key);
		$message = array(
				'html' => $html,
				'text' => $html,
				'subject' => $subject,
				'from_email' => $type,
				'from_name' => SITENAME,
				'to' => array(
						array(
								'email' => $to_email,
								'name' => $to_name,
								'type' => 'to'
						)
				),
				'headers' => array('Reply-To' => kr_support_email),
				'important' => true,
				'track_opens' => true,
				'track_clicks' => true,
				'auto_text' => null,
				'auto_html' => null,
				'inline_css' => null,
				'url_strip_qs' => null,
				'preserve_recipients' => null,
				'view_content_link' => null,
				'bcc_address' => null,
				'tracking_domain' => true,
				'signing_domain' => true,
				'return_path_domain' => true,
				'merge' => true,
				'async' => false,
				'global_merge_vars' => array(
						array(
								'name' => 'merge1',
								'content' => 'merge1 content'
						)
				),
				'merge_vars' => array(
						array(
								'rcpt' => 'recipient.email@example.com',
								'vars' => array(
										array(
												'name' => 'merge2',
												'content' => 'merge2 content'
										)
								)
						)
				),
				
		);
	
		
		$result = $mandrill->messages->send($message);

		/*
		 Array
		(
				[0] => Array
				(
						[email] => recipient.email@example.com
						[status] => sent
						[reject_reason] => hard-bounce
						[_id] => abc123abc123abc123abc123abc123
				)
	
		)
		*/
	
		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			throw $e;
		}
	
}
	
	
	/*SEND GRID
	 * 
	 * 
	 * var $url = 'https://api.sendgrid.com/';
	var $user = 'khateraho';
	var $pass = 'manisha4055645';
	
	public function mail_send($to, $subject, $body, $from)
	{
		$params = array(
	
			'api_user'  => $this->user,
			'api_key'   => $this->pass,
			'to'        => $to,
			'subject'   => $subject,
			'html'      => $body,
			'text'      => $body,
			'from'      => $from,
	);
	
	
	$request =  $this->url.'api/mail.send.json';
	
	// Generate curl request
	$session = curl_init($request);
	// Tell curl to use HTTP POST
	curl_setopt ($session, CURLOPT_POST, true);
	// Tell curl that this is the body of the POST
	curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
	// Tell curl not to return headers, but do return the response
	curl_setopt($session, CURLOPT_HEADER, false);
	curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
	
	// obtain response
	$response = curl_exec($session);
	curl_close($session);
	
	// print everything out
	return $response;
	

	}*/	
}
?>