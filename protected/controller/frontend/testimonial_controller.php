<?php
$select=$db->get_all('testimonial');

if(isset($_POST['submit']))
{
	 $name=$_POST['name'];
	 $job=$_POST['job'];
	 $work_at=$_POST['work_at'];
	 $content=$_POST['content'];
	 $image=$_FILES['image'];
   $created_date=date('y-m-d h:i:s');
	 $ip_address=$_SERVER['REMOTE_ADDR'];
	 
	 
	 
	 
	 if($fv->emptyfields(array('name'=>$name),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Name</b>
                      </div>';
	 }
	 elseif(!$fv->check_alphabets($name))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Use Only Alphabets in Client Name</b>
                      </div>';
	 }
	 elseif($fv->emptyfields(array('job'=>$job),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Job</b>
                      </div>';
	 }
	 elseif($fv->emptyfields(array('work_at'=>$work_at),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter work At</b>
                      </div>';
	 }
    elseif($fv->emptyfields(array('content'=>$content),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please content</b>
                      </div>';
	 }
	 
	  else
     {

         $insert=$db->insert('testimonial',array('name'=>$name,'job'=>$job,'work_at'=>$work_at,'content'=>$content,'created_date'=>$created_date,'ip_address'=>$ip_address));
	     
	     $insert_id = $db->insert_id;
	     $handle= new upload($_FILES['image']);
	     $path=SERVER_ROOT.'/uploads/testimonial/'.$insert_id;
	      
	     if(!is_dir($path))
	     {
	         if(!file_exists($path))
	         {
	             mkdir($path);
	         }
	     }
	     $newfilename = $handle->file_new_name_body=$insert_id;
	     $ext = $handle->image_src_type;
	     $filename = $newfilename.'.'.$ext;
	      
	      
	     if ($handle->image_src_type == 'jpg' || $handle->image_src_type == 'JPEG' || $handle->image_src_type == 'jpeg' || $handle->image_src_type == 'png' || $handle->image_src_type == 'JPG')
	     {
	          
	         if ($handle->uploaded)
	         {
	              
	             $handle->Process($path);
	             if ($handle->processed)
	             {
	                  
	                 $update=$db->update('testimonial',array('image'=>$filename),array('id'=>$insert_id));
	             }
	         }
	     }
	      
	     
	 
	 }
	 if($insert)
	 {
	 
	       $session->redirect('testimonial',frontend);
	 }
	 

	 
}




if(isset($_REQUEST['del']))
{
    {

    $display_msg='<form method="POST" action="">
    <div class="alert alert-success" >
    Are you sure ? You want to delete this .
    <input type="hidden" name="del" value="'.$_REQUEST['del'].'" >
    <button name="yes" type="submit" class="btn btn-success btn-xs"  aria-hidden="true"><i class="icon-ok-sign"></i></button>
    <button name="no" type="submit" class="btn btn-danger btn-xs" aria-hidden="true"><i class="icon-remove"></i></button>
    </div>
    </form>';
    }
    if(isset($_POST['yes']))
    {
        $delete=$db->delete("testimonial",array('id'=>$_REQUEST['del']));
        

        if($delete)
        {
            $feature->rrmdir(SERVER_ROOT.'/uploads/testimonial/'.$_REQUEST['del']);
            $session->redirect('testimonial',frontend);
        }
    }
    elseif(isset($_POST['no']))
    {
        $session->redirect('testimonial',frontend);
    }

}
	 
	 ?>