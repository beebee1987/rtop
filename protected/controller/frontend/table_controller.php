<?php
$get_room=$db->get_all('room');
$select1=$db->get_all('room');
$select=$db->get_all('table');

if(isset($_REQUEST['del']))
{
{
    $display_msg='<form method="POST" action="">
<div class="alert alert-success" >
Are you sure ? You want to delete this .
<input type="hidden" name="del" value="'.$_REQUEST['del'].'" >
<button name="yes" type="submit" class="btn btn-success btn-xs"  aria-hidden="true"><i class="icon-ok-sign"></i></button>
<button name="no" type="submit" class="btn btn-danger btn-xs"  aria-hidden="true"><i class="icon-remove"></i></button>
</div>
</form>';
}
if(isset($_POST['yes']))
{
    $delete=$db->delete("table",array('id'=>$_REQUEST['del']));
    

    if($delete)
    {
     $feature->rrmdir(SERVER_ROOT.'/uploads/room/'.$_REQUEST['del']);
        $session->redirect('table',frontend);
    }
}
elseif(isset($_POST['no']))
{
    $session->redirect('table',frontend);
}

}

if(isset($_REQUEST['del_id']))
{
    {
        $display_msg='<form method="POST" action="">
<div class="alert alert-success" >
Are you sure ? You want to delete this .
<input type="hidden" name="del_id" value="'.$_REQUEST['del_id'].'" >
<button name="yes" type="submit" class="btn btn-success btn-xs"  aria-hidden="true"><i class="icon-ok-sign"></i></button>
<button name="no" type="submit" class="btn btn-danger btn-xs" aria-hidden="true"><i class="icon-remove"></i></button>
</div>
</form>';
    }
    if(isset($_POST['yes']))
    {
        $delete=$db->delete("room",array('id'=>$_REQUEST['del_id']));


        if($delete)
        {
            $session->redirect('table',frontend);
        }
    }
    elseif(isset($_POST['no']))
    {
        $session->redirect('table',frontend);
    }

}
if(isset($_POST['view']))
{
    $room=$_POST['room'];
    if($room=='')
    {
        $select=$db->get_all('table');
    }
    else {
    $select=$db->get_all('table',array('new_room'=>$room));
    }
}

if(isset($_POST['submit']))
{
	 $table_no=$_POST['table_no'];
	 $available_place=$_POST['available_place'];
	 $table_pos=$_POST['table_pos'];
	 $room=$_POST['room'];
	 $new_room=$_POST['new_room'];
	 $image=$_FILES['image'];
     $created_date=date('y-m-d h:i:s');
	 $ip_address=$_SERVER['REMOTE_ADDR'];
	
	 
	 if ($fv->emptyfields(array('table_no'=>$table_no),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Table No</b>
                      </div>';
	 }
	 elseif ($fv->emptyfields(array('available_place'=>$available_place),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                      <b>Enter No of Places</b>
                      </div>';
	 }
	 elseif ($fv->emptyfields(array('table_pos'=>$table_pos),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                      <b>Enter Postion of Table</b>
                      </div>';
	 }
	 
	else if(($fv->emptyfields(array('new_room'=>$new_room),NULL))&&($room==''))
	{
		$display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Select Room or Add Room</b> 
                      </div>';
	}
	elseif (!$new_room=='' && !$room=='')
	{
	    $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Choose Only One Option </b>
                      </div>';
	}
	
	
	else 
	{
	  
	    if($room=='')
	    {  
	        if(!$db->exists('room',array('room'=>$new_room)))
	        {
	            $insert1=$db->insert('room',array('room'=>$new_room,'created_date'=>$created_date,'ip_address'=>$ip_address));
	        }
	        $insert=$db->insert('table',array('new_room'=>$new_room,'room'=>$new_room,'table_pos'=>$table_pos,'table_no'=>$table_no,'available_place'=>$available_place,'created_date'=>$created_date,'ip_address'=>$ip_address));
	       
	    }
	   else 
	   {
	    $insert=$db->insert('table',array('new_room'=>$room,'room'=>$room,'table_pos'=>$table_pos,'table_no'=>$table_no,'available_place'=>$available_place,'created_date'=>$created_date,'ip_address'=>$ip_address));
	   }
	    $insert_id = $db->insert_id;
	    $handle= new upload($_FILES['image']);
	    $path=SERVER_ROOT.'/uploads/room/'.$insert_id;
	    
	    if(!is_dir($path))
	    {
	        if(!file_exists($path))
	        {
	            mkdir($path);
	        }
	    }
	    $newfilename = $handle->file_new_name_body=$insert_id;
	    $ext = $handle->image_src_type;
	    $filename = $newfilename.'.'.$ext;
	    
	    
	    if ($handle->image_src_type == 'jpg' || $handle->image_src_type == 'JPEG' || $handle->image_src_type == 'jpeg' || $handle->image_src_type == 'png' || $handle->image_src_type == 'JPG')
	    {
	    
	        if ($handle->uploaded)
	        {
	    
	            $handle->Process($path);
	            if ($handle->processed)
	            {
	                
	                $update=$db->update('table',array('image'=>$filename),array('id'=>$insert_id));
	            }
	        }
	    }
	    
	    if($db->exists('room',array('room'=>$room))) {}
	    	
	    else
	    {
	       
	    
	    }
	
	}	

					if($insert)
					{
					   
						$display_msg='<div class="alert alert-success">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Form Submitted Successfully</b> 
                      </div>';
					}
		
}
?>




