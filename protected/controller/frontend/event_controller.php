<?php
$select=$db->get_all('event');

if(isset($_POST['submit']))
{
	 $event_name=$_POST['event_name'];
	 $post_by=$_POST['post_by'];
	 $event_loc=$_POST['event_loc'];
	 $event_des=$_POST['event_des'];
	 $date=$_POST['date'];
	  $time=$_POST['time'];
	 $image=$_FILES['image'];
     $created_date=date('y-m-d h:i:s');
	 $ip_address=$_SERVER['REMOTE_ADDR'];
	 
	 
	 
	 
	 if($fv->emptyfields(array('event_name'=>$event_name),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Event Name</b>
                      </div>';
	 }
	 else if($fv->emptyfields(array('post_by'=>$post_by),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Post By</b>
                      </div>';
	 }
	 elseif($fv->emptyfields(array('event_loc'=>$event_loc),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Event Location</b>
                      </div>';
	 }
	 elseif($fv->emptyfields(array('event_des'=>$event_des),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Event Description</b>
                      </div>';
	 }
    elseif($fv->emptyfields(array('date'=>$date),NULL))
	 {
	     $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Date</b>
                      </div>';
	 }
	 
	  else
     {

         $insert=$db->insert('event',array('post_by'=>$post_by,'event_name'=>$event_name,'event_loc'=>$event_loc,'event_des'=>$event_des,'date'=>$date,'time'=>$time,'created_date'=>$created_date,'ip_address'=>$ip_address));
	     
	     $insert_id = $db->insert_id;
	     $handle= new upload($_FILES['image']);
	     $path=SERVER_ROOT.'/uploads/event/'.$insert_id;
	      
	     if(!is_dir($path))
	     {
	         if(!file_exists($path))
	         {
	             mkdir($path);
	         }
	     }
	     $newfilename = $handle->file_new_name_body=$insert_id;
	     $ext = $handle->image_src_type;
	     $filename = $newfilename.'.'.$ext;
	      
	      
	     if ($handle->image_src_type == 'jpg' || $handle->image_src_type == 'JPEG' || $handle->image_src_type == 'jpeg' || $handle->image_src_type == 'png' || $handle->image_src_type == 'JPG')
	     {
	          
	         if ($handle->uploaded)
	         {
	              
	             $handle->Process($path);
	             if ($handle->processed)
	             {
	                  
	                 $update=$db->update('event',array('image'=>$filename),array('id'=>$insert_id));
	             }
	         }
	     }
	      
	     
	 
	 }
	 if($insert)
	 {
	 
	     $display_msg='<div class="alert alert-success">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Form Submitted Successfully</b>
                      </div>';
	 }
	 

	 
}




if(isset($_REQUEST['del']))
{
    {
        $display_msg='<form method="POST" action="">
<div class="alert alert-success" >
Are you sure ? You want to delete this .
<input type="hidden" name="del" value="'.$_REQUEST['del'].'" >
<button name="yes" type="submit" class="btn btn-success btn-xs"  aria-hidden="true"><i class="icon-ok-sign"></i></button>
<button name="no" type="submit" class="btn btn-danger btn-xs"  aria-hidden="true"><i class="icon-remove"></i></button>
</div>
</form>';
    }
    if(isset($_POST['yes']))
    {
        $delete=$db->delete("event",array('id'=>$_REQUEST['del']));
        

        if($delete)
        {
            $feature->rrmdir(SERVER_ROOT.'/uploads/event/'.$_REQUEST['del']);
            $session->redirect('event',frontend);
        }
    }
    elseif(isset($_POST['no']))
    {
        $session->redirect('event',frontend);
    }

}




	 
	 
	 ?>