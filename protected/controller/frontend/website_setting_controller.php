<?php
$getdata=$db->get_row('setting',array('id'=>1));
if(isset($_POST['update']))
{
    $name=$_POST['name'];
    $phone_no=$_POST['phone_no'];
    $paypal_email=$_POST['paypal_email'];
    $enable=$_POST['enable'];
   $paypal_live=$_POST['paypal_live'];
    
    $email=$_POST['email'];
   // $longitude=$_POST['longitude'];
   // $latitude=$_POST['latitude'];
    $address=$_POST['address'];
    $currency=$_POST['currency'];
    $description=$_POST['description'];
    $title=$_POST['title'];
    $keywords=$_POST['keywords'];
    $mon=$_POST['mon'];
    $tues=$_POST['tues'];
    $wed=$_POST['wed'];
    $thurs=$_POST['thurs'];
    $fri=$_POST['fri'];
    $sat=$_POST['sat'];
    $sun=$_POST['sun'];
    $ip_address=$_SERVER['REMOTE_ADDR'];
    $logo=$_FILES['logo'];
    
    $handle= new upload($_FILES['logo']);
    $path=SERVER_ROOT.'/uploads/logo/';
    if(!is_dir($path))
    {
        if(!file_exists($path))
        {
            mkdir($path);
        }
    }
    
    
    if($fv->emptyfields(array('name'=>$name),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Restaurant Name</b>
                      </div>';
    }
    
    elseif($fv->emptyfields(array('phone_no'=>$phone_no),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Restaurant Number</b>
                      </div>';
    }
    elseif($fv->emptyfields(array('paypal_email'=>$paypal_email),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Paypal E-Mail</b>
                      </div>';
    }
    elseif($fv->emptyfields(array('email'=>$email),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter E-Mail</b>
                      </div>';
    }
    elseif($fv->emptyfields(array('currency'=>$currency),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Currency</b>
                      </div>';
    }
 /*   elseif($fv->emptyfields(array('latitude'=>$latitude),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Latitude</b>
                      </div>';
    }
    elseif($fv->emptyfields(array('longitude'=>$longitude),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Longitude</b>
                      </div>';
    } */
    elseif($fv->emptyfields(array('address'=>$address),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter E-Mail</b>
                      </div>';
    }
    elseif($fv->emptyfields(array('mon'=>$mon),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Monday Opening Time</b>
                      </div>';
    }
    elseif($fv->emptyfields(array('tues'=>$tues),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Tuesday Opening Time</b>
                      </div>';
    }
    elseif($fv->emptyfields(array('wed'=>$wed),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Wednesday Opening Time</b>
                      </div>';
    }
    elseif($fv->emptyfields(array('thurs'=>$thurs),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Thursday Opening Time</b>
                      </div>';
    }
    elseif($fv->emptyfields(array('fri'=>$fri),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Friday Opening Time</b>
                      </div>';
    }
    elseif($fv->emptyfields(array('sat'=>$sat),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Saturday Opening Time</b>
                      </div>';
    }
    elseif($fv->emptyfields(array('sun'=>$sun),NULL))
    {
        $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Sunday Opening Time</b>
                      </div>';
    }
    elseif(($logo['name']) != '')
    {
        if(file_exists(SERVER_ROOT.'/uploads/logo/'.$getdata['logo']) && (($getdata['logo'])!=''))
        {
            unlink(SERVER_ROOT.'/uploads/logo/'.$getdata['logo']);
    
        }
    
        $newfilename = $handle->file_new_name_body='logo';
        $ext = $handle->image_src_type;
        $filename = $newfilename.'.'.$ext;
    
    
        if ($handle->image_src_type == 'jpg' || $handle->image_src_type == 'jpeg' || $handle->image_src_type == 'JPEG' || $handle->image_src_type == 'png' || $handle->image_src_type == 'JPG')
        {
    
    
            if ($handle->uploaded)
            {
                $handle->Process($path);
                if ($handle->processed)
                {
                   
                    $update=$db->update('setting',array('paypal_live'=>$paypal_live,'enable'=>$enable,'logo'=>$filename,'currency'=>$currency,'name'=>$name,'phone_no'=>$phone_no,'paypal_email'=>$paypal_email,'email'=>$email,'address'=>$address,'title'=>$title,'description'=>$description,'keywords'=>$keywords,'mon'=>$mon,'tues'=>$tues,'wed'=>$wed,'thurs'=>$thurs,'fri'=>$fri,'sat'=>$sat,'sun'=>$sun,'ip_address'=>$ip_address),array('id'=>1));
    
                }
            }
        }
    
    }
    
    
    
    
    
    
    else
    {
       
       $update=$db->update('setting',array('paypal_live'=>$paypal_live,'enable'=>$enable,'currency'=>$currency,'name'=>$name,'phone_no'=>$phone_no,'paypal_email'=>$paypal_email,'email'=>$email,'address'=>$address,'title'=>$title,'description'=>$description,'keywords'=>$keywords,'mon'=>$mon,'tues'=>$tues,'wed'=>$wed,'thurs'=>$thurs,'fri'=>$fri,'sat'=>$sat,'sun'=>$sun,'ip_address'=>$ip_address),array('id'=>1));
    }
    if($update)
    {
    
        $display_msg='<div class="alert alert-success">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Form Updated Successfully</b>
                      </div>';
    }
    
    
}