<?php
$get_category=$db->get_all('category');
$select=$db->get_all('order');
$sum=$db->run("SELECT sum(total) FROM `order`")->fetchColumn();
$get_client=$db->get_all('user',array('role'=>'Client'));
$get_setting=$db->get_row('setting',array('id'=>1));
$get_table=$db->get_all('table');



if(isset($_POST['step2_submit']))
{
   
    $client_id=$_POST['client_id'];
	$table_id=$_POST['table_id'];
	
     $notes=$_POST['notes'];
    $payment_type=$_POST['payment_type'];

    if($payment_type=='')
    {
        $display_msg1='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Select Payment Type</b>
                      </div>';
    }
    else if($client_id=='')
    {
        $display_msg1='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Select Client</b>
                      </div>';
    }
    else {
    
    
    $insert=$db->insert('order_final',array('client_id'=>0, 'table_id'=>$table_id, 'products'=>serialize($select),'notes'=>$notes,'payment_type'=>$payment_type,'total_amount'=>$sum,'created_date'=>date('y-m-d h:i:s'),'ip_address'=>$_SERVER['REMOTE_ADDR']));
    }
$lastid=$db->insert_id;
    if($insert)
    {
        $db->run('TRUNCATE table `order`');
        $session->redirect('order_step2&orderid='.$lastid,frontend);
    }
}

if(isset($_POST['add']))
{
    $food_id=$_POST['food_id'];
    $get=$db->get_row('food',array('id'=>$food_id));
     $food_name=$get['name'];
     $price=$get['price'];
     $quantity=$_POST['quantity'];
      $total=$price*$quantity;
     $created_on=date('y-m-d h:i:s');
     $ip_address=$_SERVER['REMOTE_ADDR'];
     
     if($fv->emptyfields(array('quantity'=>$quantity),NULL))
     {
         $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Quantity</b>
                      </div>';
     }
    else {
  
        $insert=$db->insert('order',array('food_name'=>$food_name,'food_id'=>$food_id,'price'=>$price,'total'=>$total,'quantity'=>$quantity,'created_date'=>$created_on,'ip_address'=>$ip_address));
    }
    if($insert)
    {
         
        $session->redirect('order_step1',frontend);
    }
}

if(isset($_REQUEST['del']))
{
    {
        $display_msg='<form method="POST" action="">
<div class="alert alert-success" >
Are you sure ? You want to delete this .
<input type="hidden" name="del" value="'.$_REQUEST['del'].'" >
<button name="yes" type="submit" class="btn btn-success btn-xs"  aria-hidden="true"><i class="icon-ok-sign"></i></button>
<button name="no" type="submit" class="btn btn-danger btn-xs"  aria-hidden="true"><i class="icon-remove"></i></button>
</div>
</form>';
    }
    if(isset($_POST['yes']))
    {
        $delete=$db->delete("order",array('id'=>$_REQUEST['del']));
         

        if($delete)
        {
            $session->redirect('order_step1',frontend);
        }
    }
    elseif(isset($_POST['no']))
    {
        $session->redirect('order_step1',frontend);
    }

}


?>