<?php
$get_category=$db->get_all('category');
$select1=$db->get_all('category');
$select=$db->get_all('food');
$get_setting=$db->get_row('setting',array('id'=>1));
if(isset($_POST['view']))
{
    $category=$_POST['category'];
    if($category=='')
    {
        $select=$db->get_all('food');
    }
    else
    {
    $select=$db->get_all('food',array('food_category'=>$category));
    }
  

}

if(isset($_REQUEST['del']))
{
{
    $display_msg='<form method="POST" action="">
<div class="alert alert-success" >
Are you sure ? You want to delete this .
<input type="hidden" name="del" value="'.$_REQUEST['del'].'" >
<button name="yes" type="submit" class="btn btn-success btn-xs"  aria-hidden="true"><i class="icon-ok-sign"></i></button>
<button name="no" type="submit" class="btn btn-danger btn-xs"  aria-hidden="true"><i class="icon-remove"></i></button>
</div>
</form>';
}
if(isset($_POST['yes']))
{
    $delete=$db->delete("food",array('id'=>$_REQUEST['del']));
   

    if($delete)
    {
        $feature->rrmdir(SERVER_ROOT.'/uploads/food/'.$_REQUEST['del']);
        $session->redirect('foodlist',frontend);
    }
   
}
    elseif(isset($_POST['no']))
    {
       
        $session->redirect('foodlist',frontend);
    }

}

if(isset($_REQUEST['del_id']))
{
    {
        $display_msg='<form method="POST" action="">
<div class="alert alert-success" >
Are you sure ? You want to delete this .
<input type="hidden" name="del_id" value="'.$_REQUEST['del_id'].'" >
<button name="yes" type="submit" class="btn btn-success btn-xs"  aria-hidden="true"><i class="icon-ok-sign"></i></button>
<button name="no" type="submit" class="btn btn-danger btn-xs"  aria-hidden="true"><i class="icon-remove"></i></button>
</div>
</form>';
    }
    if(isset($_POST['yes']))
    {
        $delete=$db->delete("category",array('id'=>$_REQUEST['del_id']));
        

        if($delete)
        {
            $session->redirect('foodlist',frontend);
        }
    }
    elseif(isset($_POST['no']))
    {
        
        $session->redirect('foodlist',frontend);
    }

}


if(isset($_POST['submit']))
{
	 $food_category=$_POST['food_category'];
	 $category=$_POST['category'];
	 $name=$_POST['name'];
	 $detail=$_POST['detail'];
	 $price=$_POST['price'];
	 $tags=$_POST['tags'];
	 $image=$_FILES['image'];
     $created_date=date('y-m-d h:i:s');
	 $ip_address=$_SERVER['REMOTE_ADDR'];
	
	
	if(($fv->emptyfields(array('food_category'=>$food_category),NULL))&&($category==''))
	{
		$display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Type Food Category Or Choose Category</b> 
                      </div>';
	}
	
	elseif (!$food_category=='' && !$category=='')
	{
	    $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Choose Only One Option </b>
                      </div>';
	}
	
	
	elseif ($fv->emptyfields(array('name'=>$name),NULL))
	{
		$display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Name</b> 
                      </div>';
	}
	elseif ($fv->emptyfields(array('detail'=>$detail),NULL))
	{
		$display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                      <b>Enter Details</b> 
                      </div>';
	}
	elseif ($fv->emptyfields(array('price'=>$price),NULL))
	{
	    $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                      <b>Enter Price</b>
                      </div>';
	}
	else 
	{
	    if($category=='')
	    {
	         $food_category;
	        if(!$db->exists('category',array('category'=>$food_category)))
	        {
	            $insert1=$db->insert('category',array('category'=>$food_category,'created_date'=>$created_date,'ip_address'=>$ip_address));
	        }
	        $insert=$db->insert('food',array('tags'=>$tags,'food_category'=>$food_category,'category'=>$food_category,'name'=>$name,'detail'=>$detail,'price'=>$price,'created_date'=>$created_date,'ip_address'=>$ip_address));
	       
	    }
	   else 
	   {
	    $insert=$db->insert('food',array('tags'=>$tags,'food_category'=>$category,'category'=>$category,'name'=>$name,'detail'=>$detail,'price'=>$price,'created_date'=>$created_date,'ip_address'=>$ip_address));
	   }
	    $insert_id = $db->insert_id;
	    $handle= new upload($_FILES['image']);
	    $path=SERVER_ROOT.'/uploads/food/'.$insert_id;
	    
	    if(!is_dir($path))
	    {
	        if(!file_exists($path))
	        {
	            mkdir($path);
	        }
	    }
	    $newfilename = $handle->file_new_name_body=$insert_id;
	    $ext = $handle->image_src_type;
	    $filename = $newfilename.'.'.$ext;
	    
	    
	    if ($handle->image_src_type == 'jpg' || $handle->image_src_type == 'JPEG' || $handle->image_src_type == 'jpeg' || $handle->image_src_type == 'png' || $handle->image_src_type == 'JPG')
	    {
	    
	        if ($handle->uploaded)
	        {
	    
	            $handle->Process($path);
	            if ($handle->processed)
	            {
	               
	                $update=$db->update('food',array('image'=>$filename),array('id'=>$insert_id));
	            }
	        }
	    }
	   }	

					if($insert)
					{
					   
						$display_msg='<div class="alert alert-success">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Form Submitted Successfully</b> 
                      </div>';
					}
		
}
?>
