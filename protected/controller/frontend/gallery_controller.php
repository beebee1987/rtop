<?php

$get_image_category=$db->get_all('image_category');


if(isset($_POST['submit']))
{
	 $new_category=$_POST['new_category'];
	 $image_category=$_POST['image_category'];
	 $image_name=$_POST['image_name'];
	$image=$_FILES['image'];
     $created_date=date('y-m-d h:i:s');
	 $ip_address=$_SERVER['REMOTE_ADDR'];
	
	if(($fv->emptyfields(array('new_category'=>$new_category),NULL))&&($image_category==''))
	{
		$display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Type Image Category Or Choose Category</b> 
                      </div>';
	}
	
	elseif (!$new_category=='' && !$image_category=='')
	{
	    $display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Choose Only One Option</b>
                      </div>';
	}
	
	
	elseif ($fv->emptyfields(array('image_name'=>$image_name),NULL))
	{
		$display_msg='<div class="alert alert-danger">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Please Enter Image Name</b> 
                      </div>';
	}
	
	
	else 
	{
	    
	    if($image_category=='')
	    {
	        $path=SERVER_ROOT.'/uploads/gallery/'.$new_category;
	        if(!$db->exists('image_category',array('image_category'=>$new_category)))
	        {
	            $insert1=$db->insert('image_category',array('image_category'=>$new_category,'created_date'=>$created_date,'ip_address'=>$ip_address));
	        }
	       $insert=$db->insert('image_name',array('new_category'=>$new_category,'image_category'=>$new_category,'image_name'=>$image_name,'created_date'=>$created_date,'ip_address'=>$ip_address));
	       
	    }
	   
	   else 
	   {
	    $path=SERVER_ROOT.'/uploads/gallery/'.$image_category;
	    $insert=$db->insert('image_name',array('new_category'=>$image_category,'image_category'=>$image_category,'image_name'=>$image_name,'created_date'=>$created_date,'ip_address'=>$ip_address));
	   }
	  
	    $insert_id = $db->insert_id;
	   
	    $handle= new upload($_FILES['image']);
	  
	   
	    if(!is_dir($path))
	    {
	      
	        if(!file_exists($path))
	        {
	          mkdir($path);
	        }
	    }
	 $newfilename = $handle->file_new_name_body=$insert_id;
	    $ext = $handle->image_src_type;
	 $filename = $newfilename.'.'.$ext;
	    
	    
	    if ($handle->image_src_type == 'jpg' || $handle->image_src_type == 'JPEG' || $handle->image_src_type == 'jpeg' || $handle->image_src_type == 'png' || $handle->image_src_type == 'JPG')
	    {
	    
	        if ($handle->uploaded)
	        {
	    
	           $handle->Process($path);
	            if ($handle->processed)
	            {
	             
	                $update=$db->update('image_name',array('image'=>$filename),array('id'=>$insert_id));
	               
	            }
	        }
	    }
	   }	

					if($insert)
					{
					   
						$display_msg='<div class="alert alert-success">
                      <button class="close" data-dismiss="alert" type="button">X</button>
                     <b>Image Submitted Successfully</b> 
                      </div>';
					}
		
}



if(isset($_REQUEST['del']))
{
    
  
    $get=$db->get_row('image_name',array('id'=>$_REQUEST['del']));
    unlink(SERVER_ROOT.'/uploads/gallery/'.$get['image_category'].'/'.$get['image']);
   
        $delete=$db->delete("image_name",array('id'=>$_REQUEST['del']));
    
       
           $session->redirect('gallery',frontend);
       
    
 

}


?>
